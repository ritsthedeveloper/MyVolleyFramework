package com.ritesh.clientutilslibrary.ui;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Endless scroll listener to fetch more items once a threshold is reached.
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    /**
     * Recycle view manager.
     */
    private LinearLayoutManager mLinearLayoutManager;
    /**
     * First visible item position in the list.
     */
    private int firstVisibleItem = 0;
    /**
     * Number of visible item on the screen.
     */
    private int visibleItemCount = 0;
    /**
     * Number of total item in the list.
     */
    private int totalItemCount = 0;
    /**
     * Current Page number in pagination.
     */
    private int currentPage = 1;
    /**
     * The total number of items in the dataset after the last load.
     */
    private int previousTotal = 0;
    /**
     * The minimum amount of items to have below your current scroll position before loading more.
     */
    private int visibleThreshold = 2;
    /**
     * True if we are still waiting for the last set of data to load.
     */
    private boolean loading = true;

    /**
     * Set the Recycle view manager.
     *
     * @param linearLayoutManager - recycle view manager.
     */
    public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
    }

    /**
     * Constructor.
     *
     * @param linearLayoutManager - recycle view manager.
     * @param visibleThreshold    - visible threshold value.
     */
    public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager,
                                           int visibleThreshold) {
        this.mLinearLayoutManager = linearLayoutManager;
        this.visibleThreshold = visibleThreshold;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        // get the count of visible items on screen.
        visibleItemCount = recyclerView.getChildCount();
        // get the total item count in recycle view.
        totalItemCount = mLinearLayoutManager.getItemCount();
        // get the position of the first visible item.
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }

        // Logic to detect end of the list and time to load more items.
        if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {
            // End has been reached
            // Do something
            currentPage++;
            onLoadMore(currentPage);
            loading = true;
        }
    }

    public void resetCurrentPage() {
        currentPage = 1;
        firstVisibleItem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
        previousTotal = 0;
        visibleThreshold = 2;
        loading = true;
    }
    /**
     * Abstract method to be implemented for further processing.
     *
     * @param currentPage - current page in pagination.
     */
    public abstract void onLoadMore(int currentPage);
}
