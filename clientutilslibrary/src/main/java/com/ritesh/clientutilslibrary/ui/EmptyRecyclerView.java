package com.ritesh.clientutilslibrary.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * Simple RecyclerView subclass that supports providing an empty view (which
 * is displayed when the adapter has no data and hidden otherwise).
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class EmptyRecyclerView extends RecyclerView {
    /**
     * Empty view
     */
    private View mEmptyView;

    /**
     * Observer to monitor the changes in the adapter
     */
    private AdapterDataObserver mDataObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            updateEmptyView();
        }
    };

    /**
     * Constructor.
     *
     * @param context - context.
     */
    public EmptyRecyclerView(Context context) {
        super(context);
    }

    /**
     * Constructor.
     *
     * @param context - context.
     * @param attrs   - view attributes.
     */
    public EmptyRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Constructor.
     *
     * @param context  - context.
     * @param attrs    - view attributes.
     * @param defStyle - view style.
     */
    public EmptyRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Designate a view as the empty view. When the backing adapter has no
     * data this view will be made visible and the recycler view hidden.
     *
     * @param emptyView - Empty view to be set.
     */
    public void setEmptyView(View emptyView) {
        mEmptyView = emptyView;
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        if (getAdapter() != null) {
            getAdapter().unregisterAdapterDataObserver(mDataObserver);
        }
        if (adapter != null) {
            adapter.registerAdapterDataObserver(mDataObserver);
        }
        super.setAdapter(adapter);
        updateEmptyView();
    }

    /**
     * Update the visibility of empty view based on item count.
     */
    private void updateEmptyView() {
        if (mEmptyView != null && getAdapter() != null) {
            boolean showEmptyView = getAdapter().getItemCount() == 0;
            mEmptyView.setVisibility(showEmptyView ? VISIBLE : GONE);
            setVisibility(showEmptyView ? GONE : VISIBLE);
        }
    }
}
