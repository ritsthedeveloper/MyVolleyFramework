package com.ritesh.clientutilslibrary;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * SnackBar manager that manages all the snackbar operations.
 *
 * @author Ritesh Gune.
 */
public final class SnackbarClient {

    /**
     * Default constructor.
     */
    private SnackbarClient() {

    }


    /**
     * Show long length snackbar.
     *
     * @param view view
     * @param text string text to be shown.
     */
    public static void showSnackBar(View view, String text) {
        Snackbar snackbar = Snackbar.make(view, text, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    /**
     * Show snackbar with defined length.
     *
     * @param view   view
     * @param text   string text to be shown.
     * @param length the time till the snackbar should be shown.
     */
    public static void showSnackBar(View view, String text, int length) {
        Snackbar snackbar = Snackbar.make(view, text, length);
        snackbar.show();
    }

    /**
     * Show snackbar with action button.
     *
     * @param view           view
     * @param actionText     action label
     * @param text           string text to be shown.
     * @param actionListener action button click listener
     */
    public static void showSnackBar(View view, String actionText, String text,
                                    View.OnClickListener actionListener) {
        Snackbar.make(view, text, Snackbar.LENGTH_LONG)
                .setAction(actionText, actionListener)
                .setActionTextColor(Color.RED)
                .show();
    }


}
