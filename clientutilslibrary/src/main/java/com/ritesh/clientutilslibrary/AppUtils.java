package com.ritesh.clientutilslibrary;

import android.content.Context;
import android.graphics.Point;
import android.location.LocationManager;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Utility methods for Application.
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public final class AppUtils {
    /**
     * Empty string placeholder.
     */
    public static final String EMPTY = "";

    /**
     * Upper threshold after which number to be formatted.
     */
    public static final int THRESHOLD_999 = 999;
    /**
     * Upper threshold after which number to be formatted.
     */
    public static final int THRESHOLD_99 = 99;
    /**
     * Max length of formatted number.
     */
    private static final int MAX_LENGTH = 4;
    /**
     * Tag value for logging.
     */
    private static final String TAG = AppUtils.class.getCanonicalName();
    /**
     * Array of suffix used for formatting number.
     */
    private static String[] suffix = new String[]{"", "k", "m", "b", "t"};
    private static char[] c = new char[]{'k', 'm', 'b', 't'};

    /**
     * Default constructor
     */
    private AppUtils() {

    }

    /**
     * Check whether the text is null or empty.
     *
     * @param text - text to be checked.
     * @return true or false
     */
    public static boolean isNullOrEmpty(String text) {
        return text == null || text.trim().length() == 0;
    }

    /**
     * To check whether list is null or empty.
     *
     * @param list - list of objects.
     * @param <T>  - Type of objects
     * @return - true or false
     */
    public static <T> boolean isListNullOrEmpty(List<T> list) {
        return list == null || list.size() == 0;
    }

    /**
     * Gives utc timestamp in milliseconds.
     *
     * @param timestamp - UTC timestamp in seconds
     * @return UTC timestamp in milliseconds as long value
     */
    public static long getUTCTimestampInMillis(long timestamp) {
        return timestamp * Constants.ONE_SECOND_TO_MILLISECOND;
    }

    /**
     * Gives utc timestamp in milliseconds.
     *
     * @param timestamp - timestamp in milliseconds
     * @return UTC timestamp in seconds
     */
    public static long convertToUTCTimestamp(long timestamp) {
        return timestamp / Constants.ONE_SECOND_TO_MILLISECOND;
    }

    /**
     * Show a view.
     *
     * @param row The view to be worked on
     */
    public static void showViews(View row) {
        row.setVisibility(View.VISIBLE);
    }

    /**
     * Hide a view.
     *
     * @param row The view to be worked on
     */
    public static void hideViews(View row) {
        row.setVisibility(View.GONE); // if there is a empty space change it with View.GONE
    }

    /**
     * To shorten the number formatting by appending suffix such as K,M.
     *
     * @param number - number
     * @return - formatted number.
     */
    public static String formatNumber(double number) {
        String r = new DecimalFormat("##0E0").format(number);
        r = r.replaceAll("E[0-9]", suffix[Character.getNumericValue(r.charAt(r.length() - 1)) / 3]);
        while (r.length() > MAX_LENGTH || r.matches("[0-9]+\\.[a-z]")) {
            r = r.substring(0, r.length() - 2) + r.substring(r.length() - 1);
        }
        return r;
    }

    /**
     * Recursive implementation, invokes itself for each factor of a thousand, increasing the class on each invokation.
     *
     * @param number    the number to format
     * @param iteration in fact this is the class from the array c
     * @return a String representing the number n formatted in a cool looking way.
     */
    public static String formatNumber(double number, int iteration) {
        double d = ((long) number / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;//true if the decimal part is equal to 0 (then it's trimmed anyway)
        return (d < 1000 ? //this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? //this decides whether to trim the decimals
                        (int) d * 10 / 10 : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : formatNumber(d, iteration + 1));

    }

    /**
     * To shorten the number formatting by appending suffix such as K,M when number
     * is greater thn the threshold provided.
     *
     * @param number         - number
     * @param upperThreshold - threshold
     * @return - formatted number.
     */
    public static String getFormattedNumber(int number, int upperThreshold) {
        String formattedNumber = null;
        if (upperThreshold > number) {
            formattedNumber = number + "";
        } else {
            formattedNumber = formatNumber(number, 0);
        }

        return formattedNumber;
    }

    /**
     * Get the height of the Status Bar in pixels.
     *
     * @param context - context.
     * @return result in int.
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height",
                "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * Get the height of the screen of the device in pixels.
     *
     * @param context - context.
     * @return result in int.
     */
    public static int getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        //int width = size.x;
        int height = size.y;
        return height;
    }

    public static boolean isLocationServiceEnabled(Context context) {

        LocationManager lm = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);

        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return true;
        }
        return false;
    }
}
