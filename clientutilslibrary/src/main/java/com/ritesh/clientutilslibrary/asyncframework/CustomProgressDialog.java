package com.ritesh.clientutilslibrary.asyncframework;

import android.app.Dialog;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ritesh.clientutilslibrary.R;

/**
 * Custom progress dialog to be used in common async task.
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public final class CustomProgressDialog extends Dialog {

    /**
     * Progress dialog reference
     */
    private static CustomProgressDialog dialog;

    /**
     * Constructor.
     *
     * @param context - context
     */
    private CustomProgressDialog(Context context) {
        // passing app specific theme.
        this(context, R.style.CustomProgressDialog);
    }

    /**
     * Constructor.
     *
     * @param context - context
     * @param theme   - theme
     */
    private CustomProgressDialog(Context context, int theme) {
        // passing custom theme.
        super(context, theme);
    }

    /**
     * Show custom dialog with title and message only.
     *
     * @param context - context
     * @param title   - title
     * @param message - message
     * @return - Dialog instance.
     */
    public static CustomProgressDialog show(Context context, CharSequence title,
                                            CharSequence message) {
        return show(context, title, message, false);
    }

    /**
     * Show custom dialog with title and message with indeterminate status.
     *
     * @param context       - context
     * @param title         - title
     * @param message       - message
     * @param indeterminate - true if indeterminate required.
     * @return - Dialog instance.
     */
    public static CustomProgressDialog show(Context context, CharSequence title,
                                            CharSequence message, boolean indeterminate) {
        return show(context, title, message, indeterminate, false, null);
    }

    /**
     * Show custom dialog with title and message with indeterminate status.
     *
     * @param context       - context
     * @param title         - title
     * @param message       - message
     * @param indeterminate - true if indeterminate required.
     * @param cancelable    - true if cancelable.
     * @return - Dialog instance.
     */
    public static CustomProgressDialog show(Context context, CharSequence title,
                                            CharSequence message, boolean indeterminate,
                                            boolean cancelable) {
        return show(context, title, message, indeterminate, cancelable, null);
    }

    /**
     * Show custom dialog with title and message with indeterminate status and cancel listener.
     *
     * @param context        - context
     * @param title          - title
     * @param message        - message
     * @param indeterminate  - true if indeterminate required.
     * @param cancelable     - true if cancelable.
     * @param cancelListener - cancel listener.
     * @return - Dialog instance.
     */

    public static CustomProgressDialog show(Context context, CharSequence title,
                                            CharSequence message, boolean indeterminate,
                                            boolean cancelable, OnCancelListener cancelListener) {

        dialog = new CustomProgressDialog(context);
        dialog.setTitle(title);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);
        /* The next line will add the ProgressBar to the dialog. */
        dialog.addContentView(new ProgressBar(context), new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        return dialog;
    }

    @Override
    public void onBackPressed() {
        if (null != dialog) {
            dialog.cancel();
        }
        super.onBackPressed();
    }
}
