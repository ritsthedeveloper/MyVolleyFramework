package com.ritesh.clientutilslibrary.asyncframework;

/**
 * It's an Exception wrapper created for the Application, which wraps
 * the actual Exception and the Response error object for handling the
 * Exception in appropriate manner.
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class AppException extends Exception {

    /**
     * Error code for no internet.
     */
    public static final int NO_INTERNET_ERROR_CODE = 1000;
    /**
     * Timeout error code.
     */
    public static final int TIMEOUT_ERROR_CODE = 1001;
    /**
     * Error code for default cases.
     */
    public static final int DEFAULT_ERROR_CODE = 1002;
    /**
     * Response error
     */
    private String mErrorResponse;
    /**
     * Exception
     */
    private Exception mException;

    /**
     * Constructor.
     */
    public AppException() {
        super();
    }

    public AppException(String message) {
        super(message);
    }

    /**
     * Parameterised constructor to take exception message.
     *
     * @param errorResponse - Response error object.
     * @param message Message to be displayed.
     */
    public AppException(String errorResponse ,String message) {
        super(message);
        mErrorResponse = errorResponse;
    }

    /**
     * Polymorphic constructor to take throwable object which can be thrown again.
     *
     * @param cause Throwable object.
     */
    public AppException(Throwable cause) {
        super(cause);
    }

    /**
     * Polymophic constructor to take string message and throwable object as argument.
     *
     * @param message Message to display.
     * @param cause   Throwable object.
     */
    public AppException(String message, Throwable cause) {
        super(message, cause);
    }



    /**
     * Get the RidlrException for the values provided.
     *
     * @param exc        - Exception
     * @param statusCode - error code
     * @return RidlrException
     */
    public static AppException getDefaultAppException(Exception exc, int statusCode) {
        return new AppException(exc);
    }

    public Exception getException() {
        return mException;
    }

    /**
     * Set the exception method in builder pattern.
     *
     * @param exception - Exception
     * @return RidlrException.
     */
    public AppException setException(Exception exception) {
        this.mException = exception;
        return this;
    }

    public String getResponse() {
        return mErrorResponse;
    }

    @Override
    public String toString() {
        if(mException != null)
            mException.printStackTrace();
        return "AppException{" + "mException=" + mException + ", mErrorResponse=" + mErrorResponse + '}';
    }
}