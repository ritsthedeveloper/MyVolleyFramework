package com.ritesh.clientutilslibrary.asyncframework;

/**
 * Abstract Base Class for WebService.
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public abstract class BaseWs {
    /**abstract method for starting Webservice.
     * @return Object object returned */
    public abstract Object runWS();
}
