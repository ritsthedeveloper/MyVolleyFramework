package com.ritesh.clientutilslibrary.asyncframework;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import com.ritesh.clientutilslibrary.Logger;

/**
 * Base Async task to handle Async task in a common manner.
 *
 * @author Ritesh <riteshg@birdseyetech.com>
 */
public class BaseAsyncTask extends AsyncTask<BaseWs, Object, Object>
        implements DialogInterface.OnCancelListener {

    private static final String TAG_BASE_ASYNCTASK = "BaseAsyncTask";
    /**
     * Context.
     */
    private Context mContext;
    /**
     * ProgressDialog.
     */
    private CustomProgressDialog mDialog;

    /**
     * Show/Hide flag for ProgressBar.
     */
    private boolean mProgressBarFlag;

    /**
     * Constructor .
     */
    public BaseAsyncTask() {
    }

    /**
     * Parameterised constructor.
     *
     * @param context         - context.
     * @param progressBarFlag - Show/Hide flag for ProgressBar.
     */
    public BaseAsyncTask(Context context, boolean progressBarFlag) {
        mContext = context;
        mProgressBarFlag = progressBarFlag;
    }

    /**
     * To set progressbar display status.
     *
     * @param flag - true/false
     */
    public void showProgressBar(boolean flag) {
        mProgressBarFlag = flag;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Logger.writeLog(mContext, TAG_BASE_ASYNCTASK, "Pre-Execute", Logger.INFO);

        if (null != mContext && !((Activity) mContext).isFinishing() && mProgressBarFlag) {
            // Display dialog without title, message and cancel listener.
            mDialog = CustomProgressDialog.show(mContext, null, null, true, false, this);
            mDialog.show();
        }
    }

    @Override
    protected Object doInBackground(BaseWs... params) {

        Logger.writeLog(mContext, TAG_BASE_ASYNCTASK, "DoInBackground", Logger.INFO);
        try {
            if (!isCancelled()) {
                return this.runWS();
            } else {
                return null;
            }
        } catch (AppException e) {

            Logger.writeLog(mContext, TAG_BASE_ASYNCTASK,
                    "AppException caught." + e.getResponse(), Logger.ERROR);

            e.printStackTrace();
            // Get the error response object from AppException inorder to handle the error.
            String response = e.getResponse();
            return response;
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        Logger.writeLog(mContext, TAG_BASE_ASYNCTASK,
                "OnPostExecute", Logger.INFO);

        // dismiss the progressdialog.
        if (null != mDialog && mDialog.isShowing() && !((Activity) mContext).isFinishing()) {
            mDialog.dismiss();
            mDialog = null;
        }

        //TODO : process the result as an error or success based on the response code.
        // Create a common Error handler for all error cases and a common successhandler for success.
    }

    @Override
    protected void onProgressUpdate(Object... values) {
        super.onProgressUpdate(values);
        this.handleProgress(values);
    }

    @Override
    protected void onCancelled() {

        Logger.writeLog(mContext, TAG_BASE_ASYNCTASK,
                "OnCancelled", Logger.INFO);
        super.onCancelled();
    }

    /**
     * Runs the webservice.
     *
     * @return Object of the RidlrException
     * @throws AppException exception
     */
    public Object runWS() throws AppException {
        throw new AppException("Not implemented");
    }

    /**
     * Handles the webservice response.
     *
     * @param result Result of web request
     */
    public void handleProgress(Object result) {
        Log.d("BaseAsynctask", "handleProgress");
    }

    /**
     * Handles the webservice response.
     *
     * @param result Result of web request
     */
    public void handleResponse(Object result) {
        Logger.writeLog(mContext, TAG_BASE_ASYNCTASK,
                "Inside handleResponse", Logger.INFO);
    }

    /**
     * Handle the empty response in case the call is a success.
     */
    public void handleEmptyResponseWithSuccess() {

        Logger.writeLog(mContext, TAG_BASE_ASYNCTASK,
                "Inside handleEmptyResponseWithSuccess", Logger.INFO);
    }

    /**
     * Handles the no content webservice response.
     */
    public void handleNoContentResponse() {

        Logger.writeLog(mContext, TAG_BASE_ASYNCTASK,
                "Handling No contnt case : 204", Logger.INFO);
    }

    /**
     * Method to handle conection failed response.
     *
     * @param message - Message to be displayed.
     */
    public void handleConnectionFailureResponse(String message) {

        Logger.writeLog(mContext, TAG_BASE_ASYNCTASK,
                "Handling connection failure case.", Logger.INFO);
    }

    /**
     * Handles the error case.
     *
     * @param errorCode - error code.
     */
    public void handleError(Object errorCode) {
        Logger.writeLog(mContext, TAG_BASE_ASYNCTASK,
                "Handling error case", Logger.INFO);
    }

    /**
     * Cancel listener for progressdialog.
     *
     * @param dialog - dialog.
     */
    @Override
    public void onCancel(DialogInterface dialog) {

        Logger.writeLog(mContext, TAG_BASE_ASYNCTASK,
                "Progressbar cancelled.", Logger.INFO);

        if (null != dialog) {
            dialog.cancel();
            mDialog = null;
            BaseAsyncTask.this.cancel(true);
        }
    }
}
