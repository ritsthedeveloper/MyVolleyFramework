package com.ritesh.clientutilslibrary;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public final class Constants {

    /** Self-Explanatory. */
    public static final long ONE_SECOND_TO_MILLISECOND = 1000L;
}
