package com.ritesh.clientutilslibrary;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;

import java.util.regex.Pattern;

/**
 * To get all device related information.
 *
 * @author Ritesh Gune.
 */
public final class DeviceInfoProvider {

    public static final String TAG = "DeviceInfoProvider";

    /**
     * Value to be added while converting from pixel to dp.
     */
    private static final double PX_TO_DP_FACTOR = 0.5;
    /**
     * xxxhdpi value of density
     */
    private static final double DENSITY_XXXHDPI_VALUE = 4.0;
    /**
     * xxhdpi value of density
     */
    private static final double DENSITY_XXHDPI_VALUE = 3.0;
    /**
     * xhdpi value of density
     */
    private static final double DENSITY_XHDPI_VALUE = 2.0;
    /**
     * hdpi value of density
     */
    private static final double DENSITY_HDPI_VALUE = 1.5;
    /**
     * mdpi value of density
     */
    private static final double DENSITY_MDPI_VALUE = 1.0;
    /**
     * The current development codename, or the string "REL" if this is
     * a release build.
     */
    private static String codename = Build.VERSION.CODENAME;
    /**
     * The internal value used by the underlying source control to
     * represent this build.  E.g., a perforce changelist number
     * or a git hash.
     */
    private static String incremental = Build.VERSION.INCREMENTAL;
    /**
     * The user-visible version string.  E.g., "1.0" or "3.4b5".
     */
    private static String release = Build.VERSION.RELEASE;
    /**
     * The user-visible SDK version of the framework in its raw String
     * representation;
     */
    private static String sdk = Build.VERSION.SDK;
    /**
     * The user-visible SDK version of the framework; its possible
     * values are defined in {@link Build.VERSION_CODES}.
     */
    private static int sdkInt = Build.VERSION.SDK_INT;
    /**
     * Type
     */
    private static String type = "Android";
    /**
     * The end-user-visible name for the end product.
     */
    private static String model = Build.MODEL;
    /**
     * The name of the overall product.
     */
    private static String product = Build.PRODUCT;
    /**
     * The manufacturer of the product/hardware.
     */
    private static String manufacturer = Build.MANUFACTURER;
    /**
     * The consumer-visible brand with which the product/hardware will be associated, if any.
     */
    private static String brand = Build.BRAND;
    /**
     * The name of the industrial design.
     */
    private static String device = Build.DEVICE;

    /**
     * Default constructor.
     */
    private DeviceInfoProvider() {
        // Not called.
    }

    /**
     * To get the primary email id of user.
     *
     * @param context - activity context.
     * @return user's email as string.
     */

    public static String getAccountEmailId(Context context) {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        // gives multiple account
        Account[] accounts = AccountManager.get(context).getAccountsByType("com.google");

        Account account = null;
        String contactEmail = "";

        if (null != accounts && accounts.length > 0) {
            // get the primary account
            account = accounts[0];
        }

        if (null != account && emailPattern.matcher(account.name).matches()) {
            contactEmail = account.name;
        }

        Log.i(TAG, "Contact email: " + contactEmail);

        return contactEmail;
    }

    /**
     * Get the current application version.
     *
     * @param context - activity context.
     * @return app version as int.
     */
    public static int getAppVersion(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().
                    getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }

    /**
     * Convert pixels to dp.
     *
     * @param context - activity context.
     * @param px      - number of pixel as integer.
     * @return dp value as integer.
     */
    public static int pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources()
                .getDisplayMetrics();
        return (int) ((px / displayMetrics.density) + PX_TO_DP_FACTOR);
    }

    /**
     * Convert dp to pixels.
     *
     * @param context - activity context.
     * @param dp      - dp value as integer.
     * @return number of pixel as integer.
     */
    public static int dpToPx(Context context, int dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    /**
     * Get the display density of the device.
     *
     * @param context - activity context
     * @return category as string value.
     */
    public static String getDensityName(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        Log.d(TAG, "Device density :" + density);

        if (density >= DENSITY_XXXHDPI_VALUE) {
            return "xxxhdpi";
        }
        if (density >= DENSITY_XXHDPI_VALUE) {
            return "xxhdpi";
        }
        if (density >= DENSITY_XHDPI_VALUE) {
            return "xhdpi";
        }
        if (density >= DENSITY_HDPI_VALUE) {
            return "hdpi";
        }
        if (density >= DENSITY_MDPI_VALUE) {
            return "mdpi";
        }
        return "ldpi";
    }

    /**
     * Get the device id, which will be unique for device.
     * Using android id as device id.
     *
     * @param context - context
     * @return - android id as device id.
     */
    public static String getDeviceId(Context context) {
        String androidId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return androidId;
    }

    /**
     * Get the width of the screen.
     *
     * @param activity
     * @return
     */
    public static int getScreenWidth(Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    /**
     * Check the status of the GPS.
     *
     * @param context
     * @return - GPS status
     */
    public static boolean isGpsOn(Context context) {

        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * Get the current GPS provider.
     *
     * @param context
     * @return
     */
    public static String getGPSProvider(Context context) {

        LocationManager lm = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);

        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return LocationManager.GPS_PROVIDER;
        } else if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            return LocationManager.NETWORK_PROVIDER;
        }

        return "";
    }

    /**
     * Check if location is enabled.
     *
     * @param context - App context
     * @return boolean  stating if location is on/off.
     */

    public static boolean isLocationEnabled(Context context) {
        LocationManager locationManager =
                (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            //All location services are disabled
            return false;
        }
        return true;
    }

    /**
     * Get the current location based on the GPS.
     *
     * @param context
     * @return
     */
    public static Location getLocation(Context context) {

        Location location = null;

        LocationManager lm = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        } else if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        return location;
    }

    /**
     * Get the battery level in device.
     *
     * @param context - context.
     * @return battery level as int.
     */
    public static int getDeviceBatteryLevel(Context context) {
        Intent batteryIntent = context.registerReceiver(null,
                new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        return batteryIntent.getIntExtra("level", -1);
    }

    /**
     * Get the height of status bar.
     *
     * @param context
     * @return
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

}
