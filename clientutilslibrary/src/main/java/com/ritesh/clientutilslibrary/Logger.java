package com.ritesh.clientutilslibrary;

import android.content.Context;
import android.util.Log;

/**
 * Default logging class to be used in this app.
 *
 * @author Ritesh Gune.
 */
public final class Logger {

    /**
     * Verbose Logs.
     */
    public static final int VERBOSE = 0;
    /**
     * Debug Logs.
     */
    public static final int DEBUG = 1;
    /**
     * Warning Logs.
     */
    public static final int WARN = 2;
    /**
     * Error Logs.
     */
    public static final int ERROR = 3;
    /**
     * Info Logs.
     */
    public static final int INFO = 4;
    private static Context mContext;

    /**
     * private constructor so that RLog class cannot be instantiated from anywhere outside.
     */
    private Logger() {
    }

    /**
     * Check whether the logs are enabled or not.
     *
     * @param context - context.
     * @return true or false
     */
    public static boolean logStatus(Context context) {
        return BuildConfig.logEnabled;
    }

    /**
     * To write verbose logs.
     *
     * @param context Activity from where it is called.
     * @param msg     The message to be logged
     */
    public static void writeLog(Context context, String msg) {
        if (null != context) {
            mContext = context;
            writeToLog(VERBOSE, context.getClass().getCanonicalName(), msg);
        }
    }

    /**
     * To write different type of logs.
     *
     * @param context Activity from where it is called.
     * @param msg     The message to be logged.
     * @param logType The type of log.
     */
    public static void writeLog(Context context, String msg, int logType) {
        if (null != context) {
            mContext = context;
            writeToLog(logType, context.getClass().getCanonicalName(), msg);
        }
    }

    public static void writeLog(Context context, String tag, String msg, int logType) {
        if (null != context) {
            mContext = context;
            writeToLog(logType, tag, msg);
        }
    }

    /**
     * private method which actually writes logs based on its level.
     *
     * @param type Type of log.
     * @param tag  The class from where it was logged.
     * @param msg  The message to be logged.
     */
    private static void writeToLog(int type, String tag, String msg) {
        if (logStatus(mContext)) {
            switch (type) {
            case INFO:
                Log.i(tag, msg);
                break;
            case ERROR:
                Log.e(tag, msg);
                break;
            case DEBUG:
                Log.d(tag, msg);
                break;
            case VERBOSE:
                Log.v(tag, msg);
                break;
            case WARN:
                Log.w(tag, msg);
                break;
            default:
                Log.v(tag, msg);
                break;
            }
        }
    }
}
