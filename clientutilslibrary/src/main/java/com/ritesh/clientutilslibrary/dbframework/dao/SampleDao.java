package com.ritesh.clientutilslibrary.dbframework.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
@DatabaseTable(tableName = "sample")
public class SampleDao extends BaseDao {
    /**
     * users last visited timestamp.
     */
    @DatabaseField
    private long lastVisited;

    /**
     * Get users last visited timestamp.
     * @return timestamp as long
     */
    public long getLastVisited() {
        return lastVisited;
    }

    /**
     * Set users last visited timestamp.
     * @param lastVisited - timestamp
     */
    public void setLastVisited(long lastVisited) {
        this.lastVisited = lastVisited;
    }
}
