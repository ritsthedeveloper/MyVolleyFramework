package com.ritesh.clientutilslibrary.dbframework;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.ritesh.clientutilslibrary.Logger;
import com.ritesh.clientutilslibrary.dbframework.dao.SampleDao;

import java.io.File;

/**
 * The CacheDatabaseHelper class is used to manage the interaction between the java objects and
 * the database in cached memory, for e.g. it gives us a reference to the data access object (Dao).
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class CacheDatabaseHelper extends BaseDatabaseHelper {

    /**
     * name of the database file to be created in cached memory for the application.
     */
    public static final String CACHE_DATABASE_NAME = "app_cache.sqlite";

    /**
     * For any changes in cached database objects (DAO), increase the database version.
     */
    private static final int CACHE_DATABASE_VERSION = 1;

    /**
     * Maximum cache size.
     */
    private static final long MAX_SIZE = 5242880L; // 5MB - just for testing

    /**
     * Context
     */
    private Context mContext;

    /**
     * Constructor.
     *
     * @param context - activity context.
     */
    public CacheDatabaseHelper(Context context) {
        /**
         * If no specific path is provided during creating database using SQLiteOpenHelper,
         * then it will (default) store it in device's internal memory
         * ie. data/data/app_package_name/databases/dbFilename
         *
         * Here we are providing path of cache directory
         */
        super(context, context.getCacheDir().getAbsolutePath()
                + File.separator + CACHE_DATABASE_NAME, null, CACHE_DATABASE_VERSION);
        mContext = context;
        Logger.writeLog(context, "Cache Db helper", Logger.DEBUG);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        Logger.writeLog(mContext, "onCreate of Cache Db helper called ", Logger.DEBUG);

        try {

            // TODO :Create the db tables here.
            // Ritesh : max db size 64 MB - testing
//            database.setMaximumSize(67108864);

            TableUtils.createTableIfNotExists(connectionSource, SampleDao.class);

        } catch (android.database.SQLException e) {
            Logger.writeLog(mContext, "Can't create database in Cache memory....1", Logger.ERROR);
            throw new RuntimeException(e);
        } catch (java.sql.SQLException e) {
            Logger.writeLog(mContext, "Can't create database in Cache memory....2", Logger.ERROR);
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {

        Logger.writeLog(mContext, "onUpgrade of Cache Db helper called ", Logger.DEBUG);
    }
}
