package com.ritesh.clientutilslibrary.dbframework;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.ritesh.clientutilslibrary.Logger;
import com.ritesh.clientutilslibrary.dbframework.dao.SampleDao;

import java.io.File;

/**
 * The DatabaseHelper class is used to manage the interaction between the java objects and
 * the database, for e.g. it gives us a reference to the data access object (Dao).
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class DatabaseHelper extends BaseDatabaseHelper {

    /**
     * TAG name for debugging
     */
    private static final String TAG = DatabaseHelper.class.getCanonicalName();

    /**
     * name of the database file for the application
     */
    private static final String DATABASE_NAME = "app_name.sqlite";

    /**
     * For any changes in database objects (DAO), increase the database version
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * Context
     */
    private Context mContext;

    /**
     * Constructor.
     *
     * @param context - activity context.
     */
    public DatabaseHelper(Context context) {
        /**
         * If no specific path is provided during creating database using SQLiteOpenHelper,
         * then it will (default) store it in device's internal memory
         * ie. data/data/app_package_name/databases/dbFilename
         */
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;

        Logger.writeLog(context, "Normal Db helper", Logger.DEBUG);
    }

    /**
     * Create Db at the path provided.
     * eg. Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator
     * + DATABASE_NAME
     *
     * @param context      - activity context.
     * @param specificPath - path provided
     */
    public DatabaseHelper(Context context, String specificPath) {
        super(context, specificPath + File.separator + DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;

        Logger.writeLog(context, "Normal Db helper with path provided", Logger.DEBUG);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        Logger.writeLog(mContext, "onCreate of DbHelper called ", Logger.DEBUG);

        try {

            // TODO :Create the db tables here.
            // Ritesh : max db size 64 MB - testing
//            database.setMaximumSize(67108864);

            TableUtils.createTableIfNotExists(connectionSource, SampleDao.class);

        } catch (android.database.SQLException e) {
            Logger.writeLog(mContext, "Can't create database....1", Logger.ERROR);
            throw new RuntimeException(e);
        } catch (java.sql.SQLException e) {
            Logger.writeLog(mContext, "Can't create database....2", Logger.ERROR);
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        Logger.writeLog(mContext, "onUpgrade of DbHSelper called ", Logger.DEBUG);
    }
}
