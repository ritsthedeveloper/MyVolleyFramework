package com.ritesh.clientutilslibrary.dbframework;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.ritesh.clientutilslibrary.Logger;

/**
 * BaseDatabaseHelper providing the generic/common code.
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public abstract class BaseDatabaseHelper extends OrmLiteSqliteOpenHelper {

    /**
     * Context
     */
    private Context mContext;

    /**
     * Parameterised constructor.
     *
     * @param context         Associated content from the application.
     *                        This is needed to locate the database.
     * @param databaseName    Name of the database we are opening.
     * @param factory         Cursor factory or null if none.
     * @param databaseVersion Version of the database we are opening. This causes
     *                        {onUpgrade(SQLiteDatabase, int, int)} to be
     *                        called if the stored database is a different version.
     */
    public BaseDatabaseHelper(Context context, String databaseName,
                              SQLiteDatabase.CursorFactory factory, int databaseVersion) {
        super(context, databaseName, factory, databaseVersion);
        mContext = context;
    }

    /**
     * Generic method to get the DAO of the type provided.
     *
     * @param clazz  - Dao class
     * @param <T>    - Type of the Dao.
     * @param <Long> - guid as long value.
     * @return Dao object of type requested.
     */
    public <T, Long> Dao<T, Long> getDaoFor(Class<T> clazz) {

        Dao<T, Long> dao = null;

        try {
            Logger.writeLog(mContext, "getting generic dao.", Logger.DEBUG);
            dao = getDao(clazz);
        } catch (java.sql.SQLException e) {
            Logger.writeLog(mContext, "Error while getting generic dao.", Logger.ERROR);
            e.printStackTrace();
        }

        return dao;
    }
}
