package com.ritesh.clientutilslibrary.dbframework.dao;

import com.j256.ormlite.field.DatabaseField;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class BaseDao {

    /** Column name for updateTimestamp. */
    public static final String UPDATE_TIMESTAMP = "updateTimestamp";

    /** Column name for guid. */
    public static final String GUID = "guid";

    /**
     * Primary key.
     */
    @DatabaseField(id = true, unique = true, uniqueIndexName = "guid_uname", columnName = GUID)
    private String guid;

    /**
     * Json data string.
     */
    @DatabaseField
    private String jsonData;

    /**
     * Update Timestamp of the record.
     */
    @DatabaseField (columnName = UPDATE_TIMESTAMP)
    private long updateTimestamp;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public long getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(long updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    @Override
    public String toString() {
        return "BaseDao{"
                + "guid='" + guid + '\''
                + ", jsonData='" + jsonData + '\''
                + '}';
    }
}
