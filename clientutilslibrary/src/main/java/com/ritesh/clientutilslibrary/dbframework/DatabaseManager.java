package com.ritesh.clientutilslibrary.dbframework;

import android.content.Context;

import com.google.gson.Gson;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedDelete;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.ritesh.clientutilslibrary.Logger;
import com.ritesh.clientutilslibrary.dbframework.dao.BaseDao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manages database initialising and db related queries.
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public final class DatabaseManager {

    /**
     * Constant value for no database.
     */
    public static final int NO_DB = -1;
    /**
     * Constant value for Cache database.
     */
    public static final int CACHE_DB = 0;
    /**
     * Constant value for Internal database.
     */
    public static final int INTERNAL_DB = 1;
    /**
     * Key guid.
     */
    public static final String KEY_GUID = "guid";
    /**
     * Key userGuid.
     */
    public static final String KEY_USER_GUID = "userGuid";
    /**
     * Key like.
     */
    public static final String KEY_LIKE = "like";
    /**
     * Key limit.
     */
    public static final String KEY_LIMIT = "limit";
    /**
     * Key offset.
     */
    public static final String KEY_OFFSET = "offset";
    /**
     * Key order by - column name.
     */
    public static final String KEY_ORDER_BY = "order_by";
    /**
     * Key order - Ascending or descending.
     */
    public static final String KEY_ORDER = "order";
    /**
     * TAG value for filtering.
     */
    private static final String TAG = DatabaseManager.class.getCanonicalName();
    /**
     * DB manager instance.
     */
    private static DatabaseManager mDbManagerInstance;
    /**
     * Normal DB helper instance.
     */
    private DatabaseHelper mDbHelper;
    /**
     * Cache DB helper instance.
     */
    private CacheDatabaseHelper mCacheDbHelper;

    /**
     * Context
     */
    private Context mContext;

    /**
     * Constructor.
     *
     * @param ctx - activity context.
     */
    private DatabaseManager(Context ctx) {

        mContext = ctx;

        createDbAtDefaultLocation(ctx);
        createDbInCache(ctx);
    }

    /**
     * Initialise the Database.
     *
     * @param ctx - context
     */
    public static void init(Context ctx) {

        Logger.writeLog(ctx, "initialising db ", Logger.DEBUG);
        if (null == mDbManagerInstance) {
            mDbManagerInstance = new DatabaseManager(ctx.getApplicationContext());
        }
    }

    /**
     * Get the Instance of the database manager.
     *
     * @param ctx - context.
     * @return - Databasemanager instance.
     */
    public static DatabaseManager getInstance(Context ctx) {
        if (null == mDbManagerInstance) {
            mDbManagerInstance = new DatabaseManager(ctx.getApplicationContext());
        }
        return mDbManagerInstance;
    }

    /**
     * Get the DbHelper Instance.
     *
     * @return internal db helper
     */
    private DatabaseHelper getDbHelper() {
        return mDbHelper;
    }

    /**
     * Get the dbHelper instance for the type specified.
     *
     * @param dbType - type of db
     * @return Databasehelper instance of type specified.
     */
    private BaseDatabaseHelper getDbHelperForType(int dbType) {

        BaseDatabaseHelper dbHelper = null;
        Logger.writeLog(mContext, "getting db helper for type specified.", Logger.DEBUG);
        switch (dbType) {
        case CACHE_DB:
            // cache db
            dbHelper = getCacheDbHelper();
            break;
        case INTERNAL_DB:
            // local db
            dbHelper = getDbHelper();
            break;
        default:
            break;
        }

        return dbHelper;
    }

    /**
     * Get the CacheDbHelper Instance.
     *
     * @return Cache dbHelper instance.
     */
    private CacheDatabaseHelper getCacheDbHelper() {
        return mCacheDbHelper;
    }

    /**
     * Create Database at default db location provided by android.
     *
     * @param ctx - context.
     */
    public void createDbAtDefaultLocation(Context ctx) {
        // Db in internal storage
        mDbHelper = new DatabaseHelper(ctx);
    }

    /**
     * Create Database at cache directory provided by android.
     *
     * @param ctx - context.
     */
    public void createDbInCache(Context ctx) {
        // Db in Cache storage
        mCacheDbHelper = new CacheDatabaseHelper(ctx);
    }

    /**
     * Create Database at any location provided as parameter.
     *
     * @param ctx      - context
     * @param pathToDb - path to database.
     */
    public void createDbAtLocation(Context ctx, String pathToDb) {
        // Db at the path provided
        mDbHelper = new DatabaseHelper(ctx, pathToDb);
    }

    /**
     * Generic method to get all the records for the type provided.
     *
     * @param clazz  - DAO class
     * @param dbType - database type
     * @param <T>    - type of DAO
     * @return - List of DAOs
     */
    public <T> List<T> getAllData(Class<T> clazz, int dbType) {

        List<T> allDataList = null;

        try {
            Logger.writeLog(mContext, "getting generic dao list", Logger.DEBUG);
            allDataList = getDbHelperForType(dbType).getDaoFor(clazz).queryForAll();

        } catch (SQLException e) {
            Logger.writeLog(mContext, "Error while getting generic dao list.", Logger.ERROR);
            e.printStackTrace();
        }

        return allDataList;
    }

    /**
     * Generic method to get all the records for the type provided in sorted order.
     *
     * @param clazz          - DAO class
     * @param dbType         - database type
     * @param <T>            - type of DAO
     * @param orderByColName - Column name to be used for sorting
     * @param asc            - sort in ascending or descending order
     * @return - List of DAOs
     */
    public <T> List<T> getAllData(Class<T> clazz, int dbType, String orderByColName, boolean asc) {

        List<T> allDataList = null;

        try {
            Logger.writeLog(mContext, "getting generic dao list", Logger.DEBUG);
            QueryBuilder<T, Object> queryBuilder =
                    getDbHelperForType(dbType).getDaoFor(clazz).queryBuilder();
            queryBuilder.orderBy(orderByColName, asc);
            PreparedQuery<T> preparedQuery = queryBuilder.prepare();
            allDataList = getDbHelperForType(dbType).getDaoFor(clazz).query(preparedQuery);
        } catch (SQLException e) {
            Logger.writeLog(mContext, "Error while getting generic dao list.", Logger.ERROR);
            e.printStackTrace();
        }

        return allDataList;
    }


    /**
     * Generic method to get the total count of all the records for the type provided.
     *
     * @param clazz  - DAO class
     * @param dbType - database type
     * @param <T>    - type of DAO
     * @return - total number of records in the table
     */
    public <T> long getAllDataCount(Class<T> clazz, int dbType) {

        long totalCount = 0;

        try {
            Logger.writeLog(mContext, "getting generic dao number of records", Logger.DEBUG);
            totalCount = getDbHelperForType(dbType).getDaoFor(clazz).countOf();
        } catch (SQLException e) {
            Logger.writeLog(mContext, "Error while getting generic dao number of records.",
                    Logger.ERROR);
            e.printStackTrace();
        }

        return totalCount;
    }


    /**
     * Generic method to add/save the data.
     *
     * @param clazz  - DAO class
     * @param data   - DAO object with data set.
     * @param dbType - database type
     * @param <T>    - Type of DAO.
     */
    public <T> void createData(Class clazz, T data, int dbType) {

        try {
            Logger.writeLog(mContext, "creating record in db", Logger.DEBUG);
            getDbHelperForType(dbType).getDaoFor(clazz).create(data);
        } catch (SQLException e) {
            Logger.writeLog(mContext, "Error while creting record in db.", Logger.ERROR);
            e.printStackTrace();
        }
    }

    /**
     * Update record in db.
     *
     * @param clazz  - DAO class
     * @param data   - DAO object with data set.
     * @param dbType - database type
     * @param <T>    - Type of DAO.
     */
    public <T> void updateData(Class clazz, T data, int dbType) {
        try {
            Logger.writeLog(mContext, "Updating record in db - " + clazz + " - " + data, Logger.DEBUG);
            getDbHelperForType(dbType).getDaoFor(clazz).update(data);
        } catch (SQLException e) {
            Logger.writeLog(mContext, "Error while updating record in db. - " + clazz + " - " + data, Logger.ERROR);
            e.printStackTrace();
        }
    }

    /**
     * Delete the record from db.
     *
     * @param clazz  - DAO class
     * @param data   - DAO object with data set.
     * @param dbType - database type
     * @param <T>    - Type of DAO.
     * @return number of rows deleted, -1 means exception.
     */
    public <T> int deleteData(Class clazz, T data, int dbType) {
        try {
            Logger.writeLog(mContext, "Deleting record from db", Logger.DEBUG);
            return getDbHelperForType(dbType).getDaoFor(clazz).delete(data);
        } catch (SQLException e) {
            Logger.writeLog(mContext, "Error while deleting record from db.", Logger.ERROR);
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * To feth the data from db for guid provided.
     *
     * @param clazz  - DAO class
     * @param dbType - database type
     * @param guid   - guid
     * @param <T>    - Type of DAO.
     * @return List of the DAOs of the type specified.
     */
    public <T> List<T> getAllDataByGuid(Class<T> clazz, int dbType, String guid) {

        List<T> dataList = null;

        try {
            Logger.writeLog(mContext, "fetching all data for the  guid", Logger.DEBUG);
            QueryBuilder<T, Object> queryBuilder =
                    getDbHelperForType(dbType).getDaoFor(clazz).queryBuilder();
            // prepare it so it is ready for later query or iterator calls
            Where<T, Object> where = queryBuilder.where();
            SelectArg selectArg = new SelectArg();
            // define our query as 'guid = ?'
            where.eq(KEY_GUID, selectArg);
            // later we can set the select argument and issue the query
            selectArg.setValue(guid);
            PreparedQuery<T> preparedQuery = queryBuilder.prepare();
            dataList = getDbHelperForType(dbType).getDaoFor(clazz).query(preparedQuery);
        } catch (Exception e) {
            Logger.writeLog(mContext, "Error while fetching all data for the  guid", Logger.ERROR);
            e.printStackTrace();
        }

        return dataList;
    }

    /**
     * To check whether a record with guid already exists in db.
     *
     * @param clazz  - DAO class
     * @param dbType - database type
     * @param guid   - guid
     * @param <T>    - Type of DAO.
     * @return true or false
     */
    public <T> boolean guidExists(Class<T> clazz, int dbType, String guid) {

        try {
            Logger.writeLog(mContext, "checking guid exists or not", Logger.DEBUG);
            List<T> dataList = getAllDataByGuid(clazz, dbType, guid);
            if (null != dataList && dataList.size() > 0) {
                return true;
            }
        } catch (Exception e) {
            Logger.writeLog(mContext, "Error while checking guid exists or not", Logger.ERROR);
            e.printStackTrace();
            return false;
        }
        return false;
    }

    /**
     * To fetch the data based on the conditions provided for where clause.
     *
     * @param clazz     - DAO class
     * @param dbType    - database type
     * @param mapParams - com.draw.com.draw.draw.com.draw.map.draw.com.draw.map.draw.com.draw.draw.com.draw.map.draw.com.draw.map of key-values for conditions.
     * @param <T>       - Type of DAO.
     * @return List of the DAOs of the type specified.
     */
    public <T> List<T> getAllDataByConditions(Class<T> clazz, int dbType,
                                              Map<String, String> mapParams) {

        List<T> dataList = null;

        try {
            Logger.writeLog(mContext, "fetching all data for the conditions provided", Logger.DEBUG);

            QueryBuilder<T, Object> queryBuilder =
                    getDbHelperForType(dbType).getDaoFor(clazz).queryBuilder();
            if (mapParams != null && mapParams.size() > 0) {
                Where where = queryBuilder.where();
                // define our query as 'key = ?'
                for (Map.Entry<String, String> entry : mapParams.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();

                    // Note : add all keys for where-equals clause here. Also add them in
                    // If condition make sure the column name exists in the table - Ritesh. 24 Sept
                    if (KEY_GUID.equals(key) || KEY_USER_GUID.equals(key)) {
                        where.eq(key, value);
                        Logger.writeLog(mContext, "where clause with guid...", Logger.DEBUG);
                    }

                    if (KEY_LIKE.equals(key)) {
                        where.like(value, mapParams.get("like_filter"));
                        Logger.writeLog(mContext, "query with like...", Logger.DEBUG);
                    } else if (KEY_ORDER_BY.equals(key)) {
                        queryBuilder.orderBy(value, Boolean.parseBoolean(mapParams.get(KEY_ORDER)));
                        Logger.writeLog(mContext, "query with orderby...", Logger.DEBUG);
                    } else if (KEY_LIMIT.equals(key)) {
                        queryBuilder.limit(Long.parseLong(value));
                        Logger.writeLog(mContext, "query with limit...", Logger.DEBUG);
                    } else if (KEY_OFFSET.equals(key)) {
                        queryBuilder.offset(Long.parseLong(mapParams.get(KEY_OFFSET)));
                        Logger.writeLog(mContext, "query with offset...", Logger.DEBUG);
                    }
                }
            }
            // prepare it so it is ready for later query or iterator calls
            PreparedQuery<T> preparedQuery = queryBuilder.prepare();

            dataList = getDbHelperForType(dbType).getDaoFor(clazz).query(preparedQuery);

        } catch (Exception e) {
            Logger.writeLog(mContext, "Error while fetching all data for the conditions", Logger.ERROR);
            e.printStackTrace();
        }

        return dataList;
    }

    /**
     * To update particular columns in the record based on conditions.
     *
     * @param clazz     - DAO class
     * @param dbType    - database type
     * @param guid      - guid
     * @param mapParams - com.draw.com.draw.draw.com.draw.map.draw.com.draw.map.draw.com.draw.draw.com.draw.map.draw.com.draw.map of key-values for conditions.
     * @param <T>       - Type of DAO.
     */
    public <T> void conditionalUpdate(Class<T> clazz, int dbType, String guid,
                                      Map<String, String> mapParams) {

        try {
            Logger.writeLog(mContext, "Updating record for the conditions provided.", Logger.DEBUG);
            UpdateBuilder<T, Object> updateBuilder = getDbHelperForType(dbType).getDaoFor(clazz).
                    updateBuilder();
            // update the columns with the values provided.
            for (Map.Entry<String, String> entry : mapParams.entrySet()) {
                // using column as key and value as update value.
                updateBuilder.updateColumnValue(entry.getKey(), entry.getValue());
            }
            // but only update the rows where the description is some value
            updateBuilder.where().eq(KEY_GUID, guid);
            // actually perform the update
            updateBuilder.update();
        } catch (Exception e) {
            Logger.writeLog(mContext, "Error while updating the data for the conditions.",
                    Logger.ERROR);
            e.printStackTrace();
        }
    }

    /**
     * To create a record or update if already exists in DB.
     *
     * @param clazz  - DAO class
     * @param data   - DAO object with data set.
     * @param dbType - database type
     * @param guid   - guid
     * @param <T>    - Type of DAO.
     */
    public <T> void createOrUpdateData(Class clazz, T data, int dbType, String guid) {

        Logger.writeLog(mContext, "createOrUpdateData record in db - clazz " + clazz, Logger.DEBUG);
        try {
            if (guid == null || !guidExists(clazz, dbType, guid)) {
                Logger.writeLog(mContext, "createOrUpdateData record in db - create - class" + clazz, Logger.DEBUG);
                //assume we need to create it if there is no guid
                createData(clazz, data, dbType);
            } else {
                Logger.writeLog(mContext, "createOrUpdateData record in db - update - class " + clazz, Logger.DEBUG);
                updateData(clazz, data, dbType);
            }
        } catch (Exception e) {
            Logger.writeLog(mContext, "Error while createOrUpdateData record in db.", Logger.ERROR);
            e.printStackTrace();
        }
    }

    /**
     * To merge two jsons.
     *
     * @param oldJson - old json
     * @param newJson - new json
     * @param clazz   - model class
     * @return - json string.
     */
    private String jsonMerger(String oldJson, String newJson, Class clazz) {

        Gson gson = new Gson();
        Map jsonMap1 = gson.fromJson(oldJson, HashMap.class);
        Map jsonMap2 = gson.fromJson(newJson, HashMap.class);
        Map map3 = new HashMap();
        map3.putAll(jsonMap1);
        map3.putAll(jsonMap2);

        return gson.toJson(map3);
    }

    /**
     * Delete the record from table selectively based on the conditions.
     *
     * @param clazz     - DAO class
     * @param dbType    - database type
     * @param mapParams - com.draw.com.draw.draw.com.draw.map.draw.com.draw.map.draw.com.draw.draw.com.draw.map.draw.com.draw.map with conditions
     * @param <T>       - Type of DAO.
     * @return - number of rows deleted.
     */
    public <T> int deleteDataByConditions(Class<T> clazz, int dbType,
                                          Map<String, String> mapParams) {
        try {
            Logger.writeLog(mContext, "deleting data for the conditions provided", Logger.DEBUG);

            DeleteBuilder<T, Object> deleteBuilder =
                    getDbHelperForType(dbType).getDaoFor(clazz).deleteBuilder();

            if (mapParams != null) {
                Where<T, Object> where = deleteBuilder.where();
                // define our query as 'key = ?'
                for (Map.Entry<String, String> entry : mapParams.entrySet()) {

                    String key = entry.getKey();
                    String value = entry.getValue();
                    if (BaseDao.UPDATE_TIMESTAMP.equals(key)) {
                        where.lt(key, Long.parseLong(value));
                        Logger.writeLog(mContext, "where clause with updateTimestamp...", Logger.DEBUG);
                    }
                }
            }
            // prepare it so it is ready for later query or iterator calls
            PreparedDelete<T> preparedDelete = deleteBuilder.prepare();
            return getDbHelperForType(dbType).getDaoFor(clazz).delete(preparedDelete);

        } catch (Exception e) {
            Logger.writeLog(mContext, "Error while deleting data for the conditions", Logger.ERROR);
            e.printStackTrace();
        }

        return -1;
    }

    /**
     * Delete the record from table selectively based on the conditions.
     *
     * @param clazz     - DAO class
     * @param dbType    - database type
     * @param mapParams - com.draw.com.draw.draw.com.draw.map.draw.com.draw.map.draw.com.draw.draw.com.draw.map.draw.com.draw.map with conditions
     * @param <T>       - Type of DAO.
     * @return - number of rows deleted.
     */
    public <T> int deleteDataByConditions1(Class<T> clazz, int dbType,
                                           Map<String, Object> mapParams) {
        try {
            Logger.writeLog(mContext, "deleting data for the conditions provided", Logger.DEBUG);

            DeleteBuilder<T, Object> deleteBuilder =
                    getDbHelperForType(dbType).getDaoFor(clazz).deleteBuilder();

            if (mapParams != null) {
                Where<T, Object> where = deleteBuilder.where();
                // define our query as 'key = ?'
                for (Map.Entry<String, Object> entry : mapParams.entrySet()) {

                    String key = entry.getKey();
                    Object value = entry.getValue();
                    if (value instanceof List) {
                        if (BaseDao.UPDATE_TIMESTAMP.equals(key)) {
                            // List<?> genericList = (List<String>) value;
                            // List<Long> timestampList = (List<Long>) genericList;
                            // where.in(key, timestampList);
                            where.in(key, (List<Long>) value);
                        } else if (BaseDao.GUID.equals(key)) {
                            // List<Long> timestampList = (List<Long>) genericList;
                            where.in(key, (List<String>) value);
                        }
                    } else if (value instanceof String) {
                        if (BaseDao.UPDATE_TIMESTAMP.equals(key)) {
                            where.lt(key, Long.parseLong(value.toString()));
                        } else if (BaseDao.GUID.equals(key)) {
                            where.eq(key, (String) value);
                        }
                    } else if (value instanceof Long) {
                        if (BaseDao.UPDATE_TIMESTAMP.equals(key)) {
                            where.lt(key, (long) value);
                        } else if (BaseDao.GUID.equals(key)) {
                            where.eq(key, value);
                        }
                    }
                }
            }

            // prepare it so it is ready for later query or iterator calls
            PreparedDelete<T> preparedDelete = deleteBuilder.prepare();
            return getDbHelperForType(dbType).getDaoFor(clazz).delete(preparedDelete);

        } catch (Exception e) {
            Logger.writeLog(mContext, "Error while deleting data for the conditions", Logger.ERROR);
            e.printStackTrace();
        }

        return -1;
    }

    /**
     * To fetch the data based on the conditions provided for where clause.
     *
     * @param clazz     - DAO class
     * @param dbType    - database type
     * @param mapParams - com.draw.com.draw.draw.com.draw.map.draw.com.draw.map.draw.com.draw.draw.com.draw.map.draw.com.draw.map of key-values for conditions.
     * @param <T>       - Type of DAO.
     * @return List of the DAOs of the type specified.
     */
    public <T> List<T> getAllDataByLimitOffset(Class<T> clazz, int dbType,
                                               Map<String, String> mapParams) {

        List<T> dataList = null;

        try {
            Logger.writeLog(mContext, "fetching all data by limit and offset.", Logger.DEBUG);

            QueryBuilder<T, Object> queryBuilder =
                    getDbHelperForType(dbType).getDaoFor(clazz).queryBuilder();

            if (mapParams != null) {
                // define our query as 'key = ?'
                for (Map.Entry<String, String> entry : mapParams.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();

                    if (KEY_ORDER_BY.equals(key)) {
                        queryBuilder.orderBy(value, Boolean.parseBoolean(mapParams.get(KEY_ORDER)));
                        Logger.writeLog(mContext, "query with orderby...", Logger.DEBUG);
                    } else if (KEY_LIMIT.equals(key)) {
                        queryBuilder.limit(Long.parseLong(value));
                        Logger.writeLog(mContext, "query with limit...", Logger.DEBUG);
                    } else if (KEY_OFFSET.equals(key)) {
                        queryBuilder.offset(Long.parseLong(mapParams.get(KEY_OFFSET)));
                        Logger.writeLog(mContext, "query with offset...", Logger.DEBUG);
                    }
                }
            }
            // prepare it so it is ready for later query or iterator calls
            PreparedQuery<T> preparedQuery = queryBuilder.prepare();
            dataList = getDbHelperForType(dbType).getDaoFor(clazz).query(preparedQuery);

        } catch (Exception e) {
            Logger.writeLog(mContext, "Error while fetching all data with limit offset", Logger.ERROR);
            e.printStackTrace();
        }

        return dataList;
    }

}
