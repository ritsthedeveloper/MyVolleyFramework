package com.ritesh.clientutilslibrary;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Shared preference manager that manages all shared preference related operations.
 *
 * @author Ritesh Gune.
 */
public final class SharedPreferenceClient {

    /**
     * Shared preference name for App.
     */
    public static final String PREF_NAME = "APP_NAME";
    /**
     * Shared preference Mode for App.
     */
    public static final int PREF_MODE = Context.MODE_PRIVATE;

    /**
     * Default constructor
     */
    private SharedPreferenceClient() {
        //not called
    }

    /**
     * Get the Application specific sharedpreference.
     *
     * @param context - activity context
     * @return sharedpreference for the app.
     */
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, PREF_MODE);
    }

    /**
     * Get the shared preference editor.
     *
     * @param context - activity context
     * @return sharedpreference editor for the app.
     */
    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    /**
     * Store boolean value in Application specific sharedpreference.
     *
     * @param context - activity context
     * @param key     - key of the field in sharedpreference.
     * @param value   - value to be stored.
     */
    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).apply();
    }

    /**
     * Fetch boolean value from Application specific sharedpreference.
     *
     * @param context  - activity context
     * @param key      - key of the field in sharedpreference.
     * @param defValue - default value
     * @return boolean value.
     */
    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    /**
     * Store integer value in Application specific sharedpreference.
     *
     * @param context - activity context
     * @param key     - key of the field in sharedpreference.
     * @param value   - value to be stored.
     */
    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).apply();
    }

    /**
     * Read integer value from Application specific sharedpreference.
     *
     * @param context  - activity context
     * @param key      - key of the field in sharedpreference.
     * @param defValue - default value
     * @return integer value.
     */
    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    /**
     * Store String value in Application specific sharedpreference.
     *
     * @param context - activity context
     * @param key     - key of the field in sharedpreference.
     * @param value   - value to be stored.
     */
    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).apply();
    }

    /**
     * Read String value from Application specific sharedpreference.
     *
     * @param context  - activity context
     * @param key      - key of the field in sharedpreference.
     * @param defValue - default value
     * @return string value.
     */
    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    /**
     * Store float value in Application specific sharedpreference.
     *
     * @param context - activity context
     * @param key     - key of the field in sharedpreference.
     * @param value   - value to be stored.
     */
    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).apply();
    }

    /**
     * Read float value from Application specific sharedpreference.
     *
     * @param context  - activity context
     * @param key      - key of the field in sharedpreference.
     * @param defValue - default value
     * @return float value
     */
    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    /**
     * Store long value in Application specific sharedpreference.
     *
     * @param context - activity context
     * @param key     - key of the field in sharedpreference.
     * @param value   - value to be stored.
     */
    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).apply();
    }

    /**
     * Read long value from Application specific sharedpreference.
     *
     * @param context  - activity context
     * @param key      - key of the field in sharedpreference.
     * @param defValue - default value
     * @return long value
     */
    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }
}
