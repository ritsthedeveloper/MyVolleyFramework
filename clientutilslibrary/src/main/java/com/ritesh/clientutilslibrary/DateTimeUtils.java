package com.ritesh.clientutilslibrary;

import android.content.Context;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Datetime utility functions.
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public final class DateTimeUtils {

    /**
     * Minutes in an hour.
     */
    public static final int MINUTES_IN_AN_HOUR = 60;

    /**
     * Hours in a day.
     */
    public static final int HOURS_IN_A_DAY = 24;

    /**
     * Private constructor, as utility classes cannot be instantiated.
     */
    private DateTimeUtils() {
    }

    /**
     * TimeStamp sent from is unix timestamp in seconds and hence it needs to be
     * multiplied by 1000 to convert it to milliseconds to process it further at Android end.
     *
     * @param timestamp Unix timestamp.
     * @return int timestamp
     */
    public static int getStartOfDayTimestamp(long timestamp) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTimeInMillis(timestamp);
        cal.set(Calendar.HOUR_OF_DAY, 0); //set hours to zero
        cal.set(Calendar.MINUTE, 0); // set minutes to zero
        cal.set(Calendar.SECOND, 0); //set seconds to zero
        Log.i("Time", cal.getTime().toString());
        return (int) cal.getTimeInMillis() / (int) Constants.ONE_SECOND_TO_MILLISECOND;
    }

    /**
     * Get date time.
     *
     * @param milliSeconds Unix timestamp.
     * @param dateFormat   Dateformat in which we want the date back.
     * @return String in the required format.
     */
    public static String getDateTime(long milliSeconds, String dateFormat) {
        // Unix timestamp converted to milliseconds
        long dv = milliSeconds * Constants.ONE_SECOND_TO_MILLISECOND;
        Date df = new Date(dv);

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.format(df);
    }

    /**
     * Get local timestamp.
     *
     * @param milliSeconds Unix timestamp.
     * @return long timestamp in milliseconds.
     */
    public static long getLocalTimestamp(long milliSeconds) {
        // Unix timestamp converted to milliseconds
        long dv = milliSeconds * Constants.ONE_SECOND_TO_MILLISECOND;
        Date df = new Date(dv);

        return df.getTime();
    }

    /**
     * Get mins from current time.
     *
     * @param milliSeconds Unix timestamp.
     * @return long minutes from current time.
     */
    public static long getMinsFromCurrentTime(long milliSeconds) {
        // Unix timestamp converted to milliseconds
        long dv = milliSeconds * Constants.ONE_SECOND_TO_MILLISECOND;
        Date df = new Date(dv);

        Calendar calendar = Calendar.getInstance();

        long duration = df.getTime() - calendar.getTime().getTime();
        long diffInMins = TimeUnit.MILLISECONDS.toMinutes(duration);

        return diffInMins;
    }

    /**
     * Get mins from current time.
     *
     * @param milliSeconds Unix timestamp.
     * @param calTime      calendar time
     * @return long minutes from current time.
     */
    public static long getMinsFromTime(long milliSeconds, Calendar calTime) {
        // Unix timestamp converted to milliseconds
        long dv = milliSeconds * Constants.ONE_SECOND_TO_MILLISECOND;
        Date df = new Date(dv);

        long duration = df.getTime() - calTime.getTime().getTime();
        long diffInMins = TimeUnit.MILLISECONDS.toMinutes(duration);

        return diffInMins;
    }

    /**
     * get a proper string in day or hours or mins from minutes.
     *
     * @param context    Context
     * @param diffInMins mins
     * @return mins converted to proper string in hours and days.
     */
    public static String getStringFromMins(Context context, long diffInMins) {
        String returnString = "";
        if (diffInMins <= 0) {
            return context.getString(R.string.less_than_a_min);
        } else {
            if (diffInMins > MINUTES_IN_AN_HOUR) {
                long diffInHours = TimeUnit.MINUTES.toHours(diffInMins);
                if (diffInHours > HOURS_IN_A_DAY) {
                    long diffInDay = TimeUnit.MINUTES.toDays(diffInMins);
                    long dayHours = diffInMins % HOURS_IN_A_DAY;
                    long hourMins = diffInMins % HOURS_IN_A_DAY % MINUTES_IN_AN_HOUR;
                    return getDays(context, diffInDay) + " " + getHours(context, dayHours)
                            + " " + getMins(context, hourMins);
                } else {
                    long hourMins = diffInMins % MINUTES_IN_AN_HOUR;
                    return getHours(context, diffInHours) + " " + getMins(context, hourMins);
                }
            } else {
                return getMins(context, diffInMins);
            }
        }
    }

    /**
     * Get days string from the days.
     *
     * @param context Context
     * @param days    days
     * @return String formatted string.
     */
    private static String getDays(Context context, long days) {
        if (days > 1) {
            return days + " " + context.getString(R.string.days);
        } else {
            return days + " " + context.getString(R.string.day);
        }
    }

    /**
     * Get hours formatted string from the day hours.
     *
     * @param context  Context
     * @param dayHours days
     * @return String formatted string.
     */
    private static String getHours(Context context, long dayHours) {
        if (dayHours > 1) {
            return dayHours + " " + context.getString(R.string.hours);
        } else {
            return dayHours + " " + context.getString(R.string.hour);
        }
    }

    /**
     * Get Mins formatted string from the hour mins.
     *
     * @param context  Context
     * @param hourMins hourmins
     * @return String formatted string.
     */
    private static String getMins(Context context, long hourMins) {
        if (hourMins > 1) {
            return hourMins + " " + context.getString(R.string.mins);
        } else {
            return hourMins + " " + context.getString(R.string.min);
        }
    }
}
