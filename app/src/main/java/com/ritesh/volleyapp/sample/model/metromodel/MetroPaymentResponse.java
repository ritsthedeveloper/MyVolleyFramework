package com.ritesh.volleyapp.sample.model.metromodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Birdseye on 08-03-2016.
 */
public class MetroPaymentResponse {

    @SerializedName("Booking_No")
    private String bookingNo;

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }


}
