package com.ritesh.volleyapp.sample.model.pgmodel;

import java.util.List;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class PaymentOption {

    private List<String> creditCards;
    private List<String> debitCards;
    private List<BankData> banks;

    public List<String> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(List<String> creditCards) {
        this.creditCards = creditCards;
    }

    public List<String> getDebitCards() {
        return debitCards;
    }

    public void setDebitCards(List<String> debitCards) {
        this.debitCards = debitCards;
    }

    public List<BankData> getBanks() {
        return banks;
    }

    public void setBanks(List<BankData> banks) {
        this.banks = banks;
    }
}
