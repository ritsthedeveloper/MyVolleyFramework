package com.ritesh.volleyapp.sample.apprequestconfig;

import android.content.Context;

import com.android.volley.Request;
import com.ritesh.volleyapp.R;
import com.ritesh.volleyapp.sample.model.metromodel.MetroValidateCardResponse;
import com.ritesh.volleyapp.volleycode.requestconfigs.BaseWebRequestConfig;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class MetroCardValidateRequestConfig extends BaseWebRequestConfig {

    @Override
    public String getBaseUrl(Context context) {
        return context.getString(R.string.METRO_GET_BALANCE);
    }

    @Override
    public int getRequestMethodType() {
        return Request.Method.GET;
    }

    @Override
    public Class getEntityClass() {
        return MetroValidateCardResponse.class;
    }
}
