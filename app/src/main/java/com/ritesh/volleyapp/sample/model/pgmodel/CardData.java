package com.ritesh.volleyapp.sample.model.pgmodel;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class CardData {

//    {"token":"bea8eb1ad470fd84a3ec3e638b9c839b","type":"credit","name":"John Doe","number":"XXXXXXXXXXXX4245"}

    private String token;
    private String type;
    private String name;
    private String number;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
