package com.ritesh.volleyapp.sample.model.pgmodel;



import com.ritesh.volleyapp.sample.model.BaseResponse;

import java.util.List;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class PgAuthMethodResponse extends BaseResponse {

    private PgAuthMethodData data;

    public PgAuthMethodData getData() {
        return data;
    }

    public void setData(PgAuthMethodData data) {
        this.data = data;
    }

    public class PgAuthMethodData {

        private String paymentGatewayUserId;
        private List<String> authMechanisms;

        public String getPaymentGatewayUserId() {
            return paymentGatewayUserId;
        }

        public void setPaymentGatewayUserId(String paymentGatewayUserId) {
            this.paymentGatewayUserId = paymentGatewayUserId;
        }

        public List<String> getAuthMechanisms() {
            return authMechanisms;
        }

        public void setAuthMechanisms(List<String> authMechanisms) {
            this.authMechanisms = authMechanisms;
        }
    }
}
