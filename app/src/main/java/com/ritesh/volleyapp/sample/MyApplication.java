package com.ritesh.volleyapp.sample;

import android.app.Activity;
import android.app.Application;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.ritesh.clientutilslibrary.Logger;
import com.ritesh.clientutilslibrary.dbframework.DatabaseManager;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class MyApplication extends Application {

    /**
     * Thread pool size for AsyncTask.
     */
    public static final int THREAD_POOL_SIZE = 3;

    @Override
    public void onCreate() {

        super.onCreate();

        /**
         * As the orientation for all the screens would be Portrait, instead of setting it
         * for individual activity, we will be using following method
         * to set it at the application level only.
         */
        registerActivityLifecycleCallbacks(new ActivityLifecycleHelper() {

            @Override
            public void onActivityCreated(Activity activity,
                                          Bundle savedInstanceState) {

                // new activity created; force its orientation to portrait.
                activity.setRequestedOrientation(
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        });

//        initDatabase();
    }

    /**
     * Initialise the database used for app.
     *
     * @author Ritesh
     */
    private void initDatabase() {
        Logger.writeLog(getApplicationContext(), "initDatabase called", Logger.DEBUG);
        DatabaseManager.init(getApplicationContext());
    }
}
