package com.ritesh.volleyapp.sample.model.transactionhistorymodel;

/**
 * Created by Birdseye on 09-03-2016.
 */
public class TransactionHistoryData {
    String customerNo;
    String transactionId;
    String bookingNo;
    String bookingId;
    String bookingType;
}
