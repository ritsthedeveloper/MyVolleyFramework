package com.ritesh.volleyapp.sample.model.pgmodel;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class PgResponse {

    protected int code;
    protected String status;
    protected PgResponseData data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PgResponseData getData() {
        return data;
    }

    public void setData(PgResponseData data) {
        this.data = data;
    }
}
