package com.ritesh.volleyapp.sample.apprequestconfig;

import android.content.Context;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.ritesh.volleyapp.sample.model.pgmodel.PgAuthMethodResponse;
import com.ritesh.volleyapp.volleycode.requestconfigs.BaseWebRequestConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class PgAuthMethodRequestConfig extends BaseWebRequestConfig {

    @Override
    public String getBaseUrl(Context context) {
//        return context.getString(R.string.METRO_GET_BALANCE);
        return "http://192.168.1.14:82/v1/payment_gateway/account";
    }

    @Override
    public int getRequestMethodType() {
        return Request.Method.POST;
    }

    @Override
    public Class getEntityClass() {
        return PgAuthMethodResponse.class;
    }

    @Override
    public Map<String, String> getRequestHeaders() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json; charset=utf-8");
        return headers;
    }

    @Override
    public String getRequestBodyJson(Object requestObject) {
        return new Gson().toJson(requestObject);
    }
}