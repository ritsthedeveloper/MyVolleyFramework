package com.ritesh.volleyapp.sample.model;

/**
 * Base class for response with params common in all response.
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class BaseResponse {
    protected int code;
    protected String status;
    protected String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
