package com.ritesh.volleyapp.sample.model;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class FareData {

    private String fareAmount;
    private String concessionAmount;
    private String serviceCharges;
    private String serviceTax;
    private String totalAmount;
    private String administrativeFees;
    private String cardFees;

    public String getFareAmount() {
        return fareAmount;
    }

    public void setFareAmount(String fareAmount) {
        this.fareAmount = fareAmount;
    }

    public String getConcessionAmount() {
        return concessionAmount;
    }

    public void setConcessionAmount(String concessionAmount) {
        this.concessionAmount = concessionAmount;
    }

    public String getServiceCharges() {
        return serviceCharges;
    }

    public void setServiceCharges(String serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    public String getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getAdministrativeFees() {
        return administrativeFees;
    }

    public void setAdministrativeFees(String administrativeFees) {
        this.administrativeFees = administrativeFees;
    }

    public String getCardFees() {
        return cardFees;
    }

    public void setCardFees(String cardFees) {
        this.cardFees = cardFees;
    }
}
