package com.ritesh.volleyapp.sample.model.metromodel;

/**
 * Created by Birdseye on 08-03-2016.
 */
public class MetroPaymentResponseWrapper {
    //    {"status":{"Booking_No":"81090641965015447541"},"message":"Booking has been created."}

    private MetroPaymentResponse status;
    private String message;

    public MetroPaymentResponse getStatus() {
        return status;
    }

    public void setStatus(MetroPaymentResponse status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
