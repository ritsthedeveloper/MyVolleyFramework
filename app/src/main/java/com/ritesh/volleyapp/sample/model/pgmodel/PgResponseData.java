package com.ritesh.volleyapp.sample.model.pgmodel;

import java.util.List;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class PgResponseData {

    private String ridlrToken;
    private String paymentGateway;
    private float balance;

    private ProfileData profile;
    private List<CardData> savedCards;
    private PaymentOption supported;

    public String getRidlrToken() {
        return ridlrToken;
    }

    public void setRidlrToken(String ridlrToken) {
        this.ridlrToken = ridlrToken;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public ProfileData getProfile() {
        return profile;
    }

    public void setProfile(ProfileData profile) {
        this.profile = profile;
    }

    public List<CardData> getSavedCards() {
        return savedCards;
    }

    public void setSavedCards(List<CardData> savedCards) {
        this.savedCards = savedCards;
    }

    public PaymentOption getSupported() {
        return supported;
    }

    public void setSupported(PaymentOption supported) {
        this.supported = supported;
    }
}
