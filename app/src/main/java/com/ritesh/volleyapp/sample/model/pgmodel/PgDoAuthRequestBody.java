package com.ritesh.volleyapp.sample.model.pgmodel;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class PgDoAuthRequestBody {
    //    {
//        "paymentGatewayUserId": "12344",
//            "otp": "1234",
//            "paymentGateway": "citrus"
//    }
    private String paymentGatewayUserId;
    private String otp;
    private String paymentGateway;

    public String getPaymentGatewayUserId() {
        return paymentGatewayUserId;
    }

    public void setPaymentGatewayUserId(String paymentGatewayUserId) {
        this.paymentGatewayUserId = paymentGatewayUserId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }
}
