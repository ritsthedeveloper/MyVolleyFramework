package com.ritesh.volleyapp.sample.model.pgmodel;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class ProfileData {

//    "profile":{"email":"​john.doe@gmail.com​","phone":9899589889,"name":"John Doe"}

    private String email;
    private String phone;
    private String name;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
