package com.ritesh.volleyapp.sample.model.pgmodel;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class BankData {

//    { "issuerCode":"CID002", "bankName":"AXISBank" }

    private String issuerCode;
    private String bankName;

    public String getIssuerCode() {
        return issuerCode;
    }

    public void setIssuerCode(String issuerCode) {
        this.issuerCode = issuerCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
