package com.ritesh.volleyapp.sample.model.pgmodel;


import com.ritesh.volleyapp.sample.model.BaseResponse;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class PgDoAuthResponse extends BaseResponse {

    private PgResponseData data;

    public PgResponseData getData() {
        return data;
    }

    public void setData(PgResponseData data) {
        this.data = data;
    }
}
