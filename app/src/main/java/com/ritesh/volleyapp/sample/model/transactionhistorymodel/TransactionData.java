package com.ritesh.volleyapp.sample.model.transactionhistorymodel;

import android.os.Parcel;
import android.os.Parcelable;

public class TransactionData implements Parcelable {
    private String bookingNo;
    private int bookingId;
    private String bookingType;
    private double amount;
    private String bookingStatus;
    private String companyStatus;
    private String bookingDate;
    private String companyName;
    private String cardNo;
    private String tag;
    private String destination;
    private String txnId;
    private String pgCode;

    public String getPgCode() {
        return pgCode;
    }



    public String getTxnId() {
        return txnId;
    }



    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }



    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }



    public static final Parcelable.Creator<TransactionData> CREATOR = new Creator<TransactionData>() {
        public TransactionData createFromParcel(Parcel source) {
            TransactionData transactionData = new TransactionData();
            transactionData.bookingNo = source.readString();
            transactionData.bookingId = source.readInt();
            transactionData.amount = source.readDouble();
            transactionData.bookingDate = source.readString();
            transactionData.cardNo = source.readString();
            transactionData.bookingType = source.readString();
            transactionData.txnId = source.readString();
            transactionData.companyName = source.readString();
            transactionData.pgCode = source.readString();
            return transactionData;
        }

        public TransactionData[] newArray(int size) {
            return new TransactionData[size];
        }
    };

    public TransactionData(){
    }

    public TransactionData(String bookingNo, int bookingId, String bookingType, double amount, String bookingStatus, String companyStatus,
                           String bookingDate, String companyName, String cardNo, String txnId,String pgCode) {
        this.bookingNo = bookingNo;
        this.bookingId = bookingId;
        this.bookingType = bookingType;
        this.amount = amount;
        this.bookingStatus = bookingStatus;
        this.companyStatus = companyStatus;
        this.bookingDate = bookingDate;
        this.companyName = companyName;
        this.cardNo = cardNo;
        this.txnId = txnId;
        this.pgCode = pgCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel td, int i) {
        td.writeString(this.bookingNo);
        td.writeInt(this.bookingId);
        td.writeDouble(this.amount);
        td.writeString(this.bookingDate);
        td.writeString(this.cardNo);
        td.writeString(this.bookingType);
        td.writeString(this.txnId);
        td.writeString(this.companyName);
        td.writeString(this.pgCode);
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public String getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(String companyStatus) {
        this.companyStatus = companyStatus;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getBookingId() {
        return bookingId;
    }
}