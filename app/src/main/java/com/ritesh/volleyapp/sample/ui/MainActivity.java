package com.ritesh.volleyapp.sample.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.VolleyError;
import com.ritesh.volleyapp.sample.model.pgmodel.PgAuthMethodResponse;
import com.ritesh.volleyapp.sample.model.pgmodel.PgDoAuthRequestBody;
import com.ritesh.volleyapp.sample.model.pgmodel.PgDoAuthResponse;
import com.ritesh.volleyapp.sample.model.pgmodel.PgGetBalanceResponse;
import com.ritesh.volleyapp.sample.model.pgmodel.PgResponse;
import com.ritesh.volleyapp.volleycode.ResponseReceivedListener;
import com.ritesh.volleyapp.volleycode.WebRequestClient;
import com.ritesh.volleyapp.volleycode.WebRequestErrorType;
import com.ritesh.volleyapp.volleycode.WebRequestType;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements ResponseReceivedListener {

    private static final String TAG = "MainActivity";
    private WebRequestClient mWebRequestClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWebRequestClient = new WebRequestClient(this, this);

//        makePgAuthMethodReq();
//        makeGetPgBalanceReq();
//        makeAuthReq();
        getPaymentGatewaysReq();
    }

    private void makePgAuthMethodReq() {
        HashMap<String, String> queryParams = new HashMap<String, String>();
        queryParams.put("email", "somebody@domain.com");
        queryParams.put("mobile", "8888888888");
        queryParams.put("paymentGateway", "citrus");

        mWebRequestClient.prepareRequest(WebRequestType.PG_AUTH_METHODS_REQ, null, queryParams);
    }

    private void makeGetPgBalanceReq() {
        mWebRequestClient.prepareRequest(WebRequestType.PG_GET_BALANCE_REQ, null, null);
    }

    private void makeAuthReq() {

        PgDoAuthRequestBody requestBody = new PgDoAuthRequestBody();
        requestBody.setPaymentGateway("citrus");
        requestBody.setPaymentGatewayUserId("12344");
        requestBody.setOtp("1234");

        mWebRequestClient.prepareRequest(WebRequestType.PG_DO_AUTH_REQ, null, null, requestBody);
    }

    private void getPaymentGatewaysReq() {
        mWebRequestClient.prepareRequest(WebRequestType.GET_PAY_GATEWAYS_REQ, null, null);
    }

    @Override
    public void onResponseReceived(WebRequestType requestType, Object response) {
        Log.i(TAG, "RequestType :" + requestType);

        switch (requestType) {
        case PG_AUTH_METHODS_REQ:
            PgAuthMethodResponse pgAuthMethodResponse = (PgAuthMethodResponse) response;
            Log.i(TAG, "Response Code:" + pgAuthMethodResponse.getCode());
            Log.i(TAG, "PaymentGatewayUserId:" + pgAuthMethodResponse.getData().getPaymentGatewayUserId());
            break;

        case PG_GET_BALANCE_REQ:
            PgGetBalanceResponse pgGetBalanceResponse = (PgGetBalanceResponse) response;
            Log.i(TAG, "Response Code:" + pgGetBalanceResponse.getCode());
            Log.i(TAG, "Ridlr token:" + pgGetBalanceResponse.getData().getRidlrToken());
            break;

        case PG_DO_AUTH_REQ:
            PgDoAuthResponse pgAuthResponse = (PgDoAuthResponse) response;
            Log.i(TAG, "PgDoAuthResponse Response Code:" + pgAuthResponse.getCode());
            Log.i(TAG, "PgDoAuthResponse Ridlr token:" + pgAuthResponse.getData().getRidlrToken());

            break;
        case GET_PAY_GATEWAYS_REQ:
            PgResponse pgResponse = (PgResponse) response;
            Log.i(TAG, "PgResponse Response Code:" + pgResponse.getCode());
            break;
        }
    }

    @Override
    public void onErrorResponseReceived(WebRequestType requestType, WebRequestErrorType errorType, VolleyError volleyError) {

        Log.i(TAG, "RequestType :" + requestType);
        Log.i(TAG, "ErrorType :" + errorType);
        switch (requestType) {
        case PG_AUTH_METHODS_REQ:
            break;
        }
    }
}
