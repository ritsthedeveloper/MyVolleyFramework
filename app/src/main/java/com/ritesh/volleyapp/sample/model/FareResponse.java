package com.ritesh.volleyapp.sample.model;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class FareResponse {

    private String responseCode;
    private String status;
    private FareData data;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public FareData getData() {
        return data;
    }

    public void setData(FareData data) {
        this.data = data;
    }


}
