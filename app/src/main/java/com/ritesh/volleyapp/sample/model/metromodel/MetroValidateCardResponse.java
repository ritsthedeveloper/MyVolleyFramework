package com.ritesh.volleyapp.sample.model.metromodel;

import com.ritesh.volleyapp.sample.model.BaseResponse;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class MetroValidateCardResponse extends BaseResponse {

    // {"code":200,"message":"Success","productBalance":44,"maxAmountAllowed":3000,"amountAllowed":2956}
    private int productBalance;
    private int maxAmountAllowed;
    private int amountAllowed;

    public int getProductBalance() {
        return productBalance;
    }

    public void setProductBalance(int productBalance) {
        this.productBalance = productBalance;
    }

    public int getMaxAmountAllowed() {
        return maxAmountAllowed;
    }

    public void setMaxAmountAllowed(int maxAmountAllowed) {
        this.maxAmountAllowed = maxAmountAllowed;
    }

    public int getAmountAllowed() {
        return amountAllowed;
    }

    public void setAmountAllowed(int amountAllowed) {
        this.amountAllowed = amountAllowed;
    }
}
