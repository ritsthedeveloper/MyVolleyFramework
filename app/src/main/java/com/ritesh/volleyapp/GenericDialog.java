package com.ritesh.volleyapp;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class GenericDialog extends Dialog implements View.OnClickListener {

    /** constant for Positive/Ok button */
    public static final String POSITIVE_BTN_CLICKED = "POSITIVE";
    /** constant for Negative/Cancel button */
    public static final String NEGATIVE_BTN_CLICKED = "NEGATIVE";

    public static final String BACK_BTN_CLICKED = "BACK";

    /** Context */
    private Context mContext;
    /**
     * Textview for title
     */
    private TextView mTvTitle;
    /**
     * Textview for message
     */
    private TextView mTvMessage;
    /**
     * Button ok.
     */
    private Button mBtnPositive;
    /**
     * Button cancel.
     */
    private Button mBtnNegative;
    /**
     * Title for the dialog.
     */
    private String mTitle;

    /**
     * Positive/Negative click listener.
     */
    private OnButtonClickListener mBtnClickListener;

    /**
     * Interface to listen to positive/negative button click.
     */
    public interface OnButtonClickListener {
        void onButtonClick(String buttonType);
    }

    public GenericDialog(Context context) {
        super(context,  R.style.DialogTheme);
        setContentView(R.layout.dialog_generic);

        mTvTitle  = (TextView)findViewById(R.id.title);
        mTvMessage = (TextView)findViewById(R.id.line1);

        mBtnPositive = (Button) findViewById(R.id.btn_positive);
        mBtnPositive.setOnClickListener(this);

        mBtnNegative = (Button) findViewById(R.id.btn_negative);
        mBtnNegative.setOnClickListener(this);
    }

    /**
     * Set the title for Dialog.
     * @param titleText
     */
    public void setDialogTitle(String titleText){
        mTvTitle.setText(titleText);
    }

    /**
     * Message to be displayed to user.
     * @param messageText
     */
    public void setDialogMessage(String messageText){
        mTvMessage.setText(messageText);
    }

    /**
     * Text for the posititve button.
     * @param labelText
     */
    public void setPositiveButtonLabel(String labelText){
        mBtnPositive.setText(labelText);
    }

    /**
     * Text for the negative button.
     * @param labelText
     */
    public void setNegativeButtonLabel(String labelText){
        mBtnNegative.setText(labelText);
    }

    /**
     * Visibility of the negative button.
     * @param visibility
     */
    public void setNegativeBtnVisibility(int visibility){
        mBtnNegative.setVisibility(visibility);
    }

    /**
     * Set the activity/fragment as listener for further processing on button click.
     * @param listener - listener.
     */
    public void setOnButtonClickListener(OnButtonClickListener listener) {
        mBtnClickListener = listener;
    }

    @Override
    public void onClick(View v) {

        String selectedBtnType = null;

        switch (v.getId()) {
        case R.id.btn_positive:
            selectedBtnType = POSITIVE_BTN_CLICKED;
            break;
        case R.id.btn_negative:
            selectedBtnType = NEGATIVE_BTN_CLICKED;
            break;
        default:
            selectedBtnType = "";
            break;
        }

        handleButtonClick(selectedBtnType);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handleButtonClick(BACK_BTN_CLICKED);
    }

    /**
     * Handle the button click for dialog.
     * Dismiss the dialog and navigate the control back to the activity.
     *
     * @param buttonType
     */
    private void handleButtonClick(String buttonType){

        dismiss();

        if (null != mBtnClickListener) {
            mBtnClickListener.onButtonClick(buttonType);
        }
    }
}
