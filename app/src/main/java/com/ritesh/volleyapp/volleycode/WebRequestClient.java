package com.ritesh.volleyapp.volleycode;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import com.android.volley.Response;
import com.ritesh.volleyapp.CustomProgressDialog;
import com.ritesh.volleyapp.volleycode.requestconfigs.IWebRequestConfig;

import java.util.List;
import java.util.Map;

/**
 * Prepares API call with success and error listeners.
 *
 * @author Ritesh Gune.
 */
public class WebRequestClient implements DialogInterface.OnCancelListener {

    private static final String TAG = "WebRequestClient";

    private Context mContext;
    // Success and Error listener
    private ResponseReceivedListener mResponseReceivedListener;
    // Name of the current request
    private WebRequestType mRequestType;
    // ProgressDialog.
    private CustomProgressDialog mDialog;
    // Request body for Post/Put.
    private String mRequestBody;

    /**
     * Success response listener.
     */
    private Response.Listener mSuccessListener = new Response.Listener() {
        @Override
        public void onResponse(Object response) {
            if (null != mResponseReceivedListener) {
                // Dismiss the common prgrs dlg
                if (null != mDialog && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                // send back the result with the request name to the calling activity.
                mResponseReceivedListener.onResponseReceived(mRequestType, response);
            }
        }
    };

    public WebRequestClient(Context context) {
        mContext = context;
    }

    public WebRequestClient(Context context, ResponseReceivedListener responseReceivedListener) {
        this(context);
        this.mResponseReceivedListener = responseReceivedListener;
    }

    /**
     * Prepare volley request with the param values provided.
     *
     * @param requestType - request identifier.
     * @param pathParams  - additional url params to be appended to base url.
     * @param queryParams - request query params.
     */
    public void prepareRequest(WebRequestType requestType, List<String> pathParams,
                               Map<String, String> queryParams) {

        // we need to send the request name in callback.
        mRequestType = requestType;

        // Error response listener.
        VolleyErrorHelper mErrorListener = new VolleyErrorHelper(mContext, requestType, mResponseReceivedListener);
        /**
         * Dialog reference to dismiss the dialog in case of error response.
         */
        mErrorListener.setProgressDialog(mDialog);

        IWebRequestConfig requestConfig = requestType.getRequestConfig();

        VolleyRequest request = new VolleyRequest.VolleyRequestBuilder()
                .setContext(mContext)
                .setRequestBody(mRequestBody)
                .setMethodType(requestConfig.getRequestMethodType()) // get the method type for the request.
                .setUrl(requestConfig.getBaseUrl(mContext)) // get the base url for the request.
                .setClazz(requestConfig.getEntityClass()) // get the response class for the request.
                .setHeaders(requestConfig.getRequestHeaders()) // get the request headers
                .setQueryParams(queryParams)
                .setPathParams(pathParams)
                .setListener(mSuccessListener)
                .setErrorListener(mErrorListener)
                .createVolleyRequest();

        request.setPriority(requestConfig.getRequestPriority());

        // Add the request to the queue with specific tag.
        VolleyRequestQueue.getInstance(mContext.getApplicationContext())
                .addToRequestQueue(request, String.valueOf(requestType));

        // Display common progress bar for all requests.
        if (null != mContext && !((Activity) mContext).isFinishing()) {
            // Display dialog without title, message and cancel listener.
            mDialog = CustomProgressDialog.show(mContext, null, null, true, false, this);
            mDialog.show();
        }
    }

    public void prepareRequest(WebRequestType requestType, List<String> pathParams,
                               Map<String, String> queryParams,Object requestBody) {

        mRequestBody = requestType.getRequestConfig().getRequestBodyJson(requestBody);
        prepareRequest(requestType,pathParams,queryParams);
    }

    /**
     * Cancel listener for progressdialog.
     *
     * @param dialog - dialog.
     */
    @Override
    public void onCancel(DialogInterface dialog) {

        if (null != dialog) {
            // dismiss the common progress dialog.
            dialog.cancel();
            mDialog = null;
            // cancel the request
            VolleyRequestQueue.getInstance(mContext.getApplicationContext()).
                    cancelPendingRequests(String.valueOf(mRequestType));
        }
    }
}

