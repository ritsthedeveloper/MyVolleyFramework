package com.ritesh.volleyapp.volleycode;

/**
 * Error types.
 */
public enum WebRequestErrorType {

    NO_INTERNET_ERROR(1),
    TIMEOUT_ERROR(2),
    SERVER_ERROR(3),
    NETWORK_ERROR(4),
    GENERIC_ERROR(5);

    private int errorConstant;

    WebRequestErrorType(int errorConstant) {
        this.errorConstant = errorConstant;
    }

    public int getErrorConstant() {
        return this.errorConstant;
    }
}
