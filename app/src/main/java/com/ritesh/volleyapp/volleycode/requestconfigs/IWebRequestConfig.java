package com.ritesh.volleyapp.volleycode.requestconfigs;

import android.content.Context;

import com.android.volley.Request;

import java.util.Map;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public interface IWebRequestConfig {

    String getBaseUrl(Context context);

    int getRequestMethodType();

    Class getEntityClass();

    Request.Priority getRequestPriority();

    Map<String, String> getRequestHeaders();

    String getRequestBodyJson(Object requestObject);
}
