package com.ritesh.volleyapp.volleycode;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.ritesh.volleyapp.UrlBuilder;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Volley adapter for JSON requests that will be parsed into Java objects by Gson.
 *
 * @author Ritesh Gune.
 */
public class VolleyRequest<T> extends Request<T> {

    private static final String TAG = "VolleyRequest";

    private static final String PROTOCOL_CONTENT_TYPE = String.format("application/json; charset=%s", new Object[]{"utf-8"});

    // gson for parsing jsonresponse
    private final Gson gson = new Gson();
    // Pojo class for response
    private final Class<T> clazz;
    // request headers
    private final Map<String, String> headers;
    // Url path parameteres
    private final List<String> mPathParams;
    // Query parameters for request
    private final Map<String, String> mQueryParams;
    // Success response listener
    private final Listener<T> listener;
    // Error response listener
    private final ErrorListener mErrorListener;
    // Request priority
    private Priority mPriority = Priority.NORMAL;

    private Context mContext;
    // Request Url
    private String mUrl;
    // Request body for Post/Put
    private String mRequestBody;
    // Request method type
    private int mMethodType;
    // Flag to check whether response was from cache or network.
    private boolean mCacheHit;

    /**
     * Make a GET/POST/PUT/DELETE request and return a parsed object from JSON.
     */

    private VolleyRequest(VolleyRequestBuilder requestBuilder) {
        super(requestBuilder.methodType, requestBuilder.url, requestBuilder.errorListener);
        mContext = requestBuilder.context;
        mMethodType = requestBuilder.methodType;
        mUrl = requestBuilder.url;
        mRequestBody = requestBuilder.requestBody;
        this.clazz = requestBuilder.clazz;
        this.headers = requestBuilder.headers;
        mPathParams = requestBuilder.pathParams;
        mQueryParams = requestBuilder.queryParams;
        this.listener = requestBuilder.listener;
        mErrorListener = requestBuilder.errorListener;

        if (Method.GET == mMethodType) {
            buildGetUrl();
        }
    }

    private void buildGetUrl() {
        Log.i(TAG, "building get url.");
        UrlBuilder urlBuilder = new UrlBuilder(mContext);
        mUrl = urlBuilder.buildUrl(mContext, mUrl, mPathParams, mQueryParams);
    }

    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @Override
    public String getUrl() {
        return mUrl;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json; charset=utf-8");
        headers.put("apk_key", "A*S)(CR@!T^ST%%C**!l@N#T$I");
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        // if is just for debugging
        if (null != mQueryParams) {
            Log.i(TAG, "Param size :" + mQueryParams.size());
            Iterator it = mQueryParams.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                Log.d(TAG, "Param - Key :" + pair.getKey() + " :::: Value : " + pair.getValue());
            }

            Log.d(TAG, "-----------------------------------------------");
        }
        return mQueryParams != null ? mQueryParams : super.getParams();
    }

    /**
     * Request body for post.
     * @return
     * @throws AuthFailureError
     */
    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return mRequestBody == null?null:mRequestBody.getBytes("utf-8");
        } catch (UnsupportedEncodingException var2) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    new Object[]{mRequestBody, "utf-8"});
            return null;
        }
    }

    @Override
    protected void deliverResponse(T response) {
        if (null != listener) {
            listener.onResponse(response);
        }
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {

        try {
            String json = new String(
                    response.data, HttpHeaderParser.parseCharset(response.headers));
            Log.i(TAG,"Volley ResponseJsonString :"+json);
            return Response.success(
                    gson.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        } catch (Exception e) {
            return Response.error(new VolleyError(e));
        }
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        // here you can write a custom retry policy
        return super.getRetryPolicy();
    }

    @Override
    public Priority getPriority() {
        return mPriority;
    }

    //Change the request priority at runtime.
    public void setPriority(Priority requestPriority) {
        mPriority = requestPriority;
    }

    /**
     * To check whether Volley gets the results from the cache or from the network.
     *
     * @param tag
     */

    @Override
    public void addMarker(String tag) {
        super.addMarker(tag);
        if (null != tag && tag.equals("cache-hit")) {
            mCacheHit = true;
        }
    }

    public static class VolleyRequestBuilder<T> {

        private Context context;
        private int methodType;
        private String url;
        private String requestBody;
        private Class<T> clazz;
        private Map<String, String> headers;
        private List<String> pathParams;
        private Map<String, String> queryParams;
        private Response.Listener<T> listener;
        private Response.ErrorListener errorListener;

        public VolleyRequestBuilder setContext(Context context) {
            this.context = context;
            return this;
        }

        /**
         * @param methodType - type of the method eg GET,POST etc
         * @return
         */
        public VolleyRequestBuilder setMethodType(int methodType) {
            this.methodType = methodType;
            return this;
        }

        /**
         * @param url - URL of the request to make
         * @return
         */
        public VolleyRequestBuilder setUrl(String url) {
            this.url = url;
            return this;
        }

        /**
         *
         * @param requestBody
         * @return
         */
        public VolleyRequestBuilder setRequestBody(String requestBody){
            this.requestBody = requestBody;
            return this;
        }

        /**
         * @param clazz - Relevant class object, for Gson's reflection
         * @return
         */
        public VolleyRequestBuilder setClazz(Class<T> clazz) {
            this.clazz = clazz;
            return this;
        }

        /**
         * @param headers - Map of request headers
         * @return
         */
        public VolleyRequestBuilder setHeaders(Map<String, String> headers) {
            this.headers = headers;
            return this;
        }

        /**
         * @param pathParams - Map of path params in case of additional params to base url
         * @return
         */
        public VolleyRequestBuilder setPathParams(List<String> pathParams) {
            this.pathParams = pathParams;
            return this;
        }

        public VolleyRequestBuilder setQueryParams(Map<String, String> queryParams) {
            this.queryParams = queryParams;
            return this;
        }

        /**
         * @param listener - Success response listener
         * @return
         */
        public VolleyRequestBuilder setListener(Response.Listener<T> listener) {
            this.listener = listener;
            return this;
        }

        /**
         * @param errorListener - Error listener
         * @return
         */
        public VolleyRequestBuilder setErrorListener(Response.ErrorListener errorListener) {
            this.errorListener = errorListener;
            return this;
        }

        public VolleyRequest createVolleyRequest() {
            return new VolleyRequest(this);
        }
    }
}