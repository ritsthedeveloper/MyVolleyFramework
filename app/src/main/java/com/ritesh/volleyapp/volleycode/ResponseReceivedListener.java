package com.ritesh.volleyapp.volleycode;

import com.android.volley.VolleyError;

/**
 * Callback for response/error for the request made.
 *
 * @author Ritesh Gune.
 */
public interface ResponseReceivedListener {

    // success case
    void onResponseReceived(WebRequestType requestType, Object response);

    // error case
    void onErrorResponseReceived(WebRequestType requestType,WebRequestErrorType errorType,
                                 VolleyError volleyError);
}