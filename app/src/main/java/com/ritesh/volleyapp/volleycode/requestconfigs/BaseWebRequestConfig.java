package com.ritesh.volleyapp.volleycode.requestconfigs;

import android.content.Context;

import com.android.volley.Request;
import com.ritesh.volleyapp.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides default values for some of the methods.
 *
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public abstract class BaseWebRequestConfig implements IWebRequestConfig {

    public String getBaseUrl(Context context) {
        // default base url
        return context.getString(R.string.API_URL);
    }

    public abstract int getRequestMethodType();

    public abstract Class getEntityClass();

    public Request.Priority getRequestPriority() {
        // default priority for request.
        return Request.Priority.NORMAL;
    }

    public Map<String, String> getRequestHeaders() {

        // Default headers for request.
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json; charset=utf-8");
        headers.put("apk_key", "A*S)(CR@!T^ST%%C**!l@N#T$I");

        return headers;
    }

    @Override
    public String getRequestBodyJson(Object requestObject) {
        return null;
    }
}
