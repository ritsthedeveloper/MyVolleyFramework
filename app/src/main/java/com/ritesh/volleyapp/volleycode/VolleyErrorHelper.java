package com.ritesh.volleyapp.volleycode;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ritesh.clientutilslibrary.SnackbarClient;
import com.ritesh.volleyapp.CustomProgressDialog;
import com.ritesh.volleyapp.GenericDialog;
import com.ritesh.volleyapp.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Error Interpreter/Handler class for volley requests.
 * Common error handling behaviour is implemented here.
 * Also the error is sent back to calling class in case
 * specific behaviour is to be implemented.
 *
 * @author Ritesh Gune.
 */
public class VolleyErrorHelper implements Response.ErrorListener {

    private static final String TAG = "VolleyErrorHelper";

    private Context mContext;
    // Error listener implemented in calling class.
    private ResponseReceivedListener mErrorListener;
    // Type of the request for which error occurred.
    private WebRequestType mRequestType;
    // Type of the error.
    private WebRequestErrorType mErrorType;
    // Actual error.
    private VolleyError mVolleyError;
    // Common dialog to display error message.
    private GenericDialog mGenericDialog;
    // ProgressDialog.
    private CustomProgressDialog mProgressDialog;
    /**
     * Listener to listen the ok/cancel button click of dialog.
     */
    private GenericDialog.OnButtonClickListener mFailureDlgListener =
            new GenericDialog.OnButtonClickListener() {

                @Override
                public void onButtonClick(String buttonType) {

                    if (GenericDialog.POSITIVE_BTN_CLICKED.equals(buttonType)) {
                        if (null != mErrorListener) {
                            mErrorListener.onErrorResponseReceived(mRequestType, mErrorType, mVolleyError);
                        }
                    }
                }
            };

    public VolleyErrorHelper(Context context, WebRequestType requestType,
                             ResponseReceivedListener errorListener) {
        mContext = context;
        mRequestType = requestType;
        mErrorListener = errorListener;
    }

    /**
     * Returns appropriate message which is to be displayed to the user
     * against the specified error object.
     *
     * @param error
     * @param context
     * @return
     */
    public static String getMessage(Object error, Context context) {
        if (error instanceof TimeoutError) {
            return context.getResources().getString(R.string.generic_server_down);
        } else if (isServerProblem(error)) {
            return handleServerError(error, context);
        } else if (isNetworkProblem(error)) {
            return context.getResources().getString(R.string.no_internet);
        }
        return context.getResources().getString(R.string.generic_error_message);
    }

    /**
     * Determines whether the error is related to network.
     *
     * @param error
     * @return
     */
    private static boolean isNetworkProblem(Object error) {
        return (error instanceof NetworkError) || (error instanceof NoConnectionError);
    }

    /**
     * Determines whether the error is related to server.
     *
     * @param error
     * @return
     */
    private static boolean isServerProblem(Object error) {
        return (error instanceof ServerError) || (error instanceof AuthFailureError);
    }

    /**
     * Handles the server error, tries to determine whether to show a stock message or to
     * show a message retrieved from the server.
     *
     * @param err
     * @param context
     * @return
     */
    private static String handleServerError(Object err, Context context) {

        VolleyError error = (VolleyError) err;
        NetworkResponse response = error.networkResponse;

        if (response != null) {
            switch (response.statusCode) {
            case 404:
            case 422:
            case 401:
                try {
                    // server might return error like this { "error": "Some error occured" }
                    // Use "Gson" to parse the result
                    HashMap<String, String> result = new Gson().fromJson(new String(response.data),
                            new TypeToken<Map<String, String>>() {
                            }.getType());

                    if (result != null && result.containsKey("error")) {
                        return result.get("error");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                // invalid request
                return error.getMessage();

            default:
                return context.getResources().getString(R.string.generic_server_down);
            }
        }
        return context.getResources().getString(R.string.generic_error_message);
    }

    /**
     * Progress dialog reference to dismiss the ProgressBar in error cases.
     *
     * @param ongoingDialog
     */
    public void setProgressDialog(CustomProgressDialog ongoingDialog) {
        mProgressDialog = ongoingDialog;
    }

    /**
     * Returns appropriate type of error so as to handle case accordingly.
     *
     * @param error
     * @return
     */
    private WebRequestErrorType getmErrorType(VolleyError error) {

        if (error instanceof NoConnectionError) {
            return WebRequestErrorType.NO_INTERNET_ERROR;
        } else if (error instanceof TimeoutError) {
            return WebRequestErrorType.TIMEOUT_ERROR;
        } else if (isServerProblem(error)) {
            return WebRequestErrorType.SERVER_ERROR;
        } else if (error instanceof NetworkError) {
            return WebRequestErrorType.NETWORK_ERROR;
        } else {
            return WebRequestErrorType.GENERIC_ERROR;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

        mVolleyError = error;
        handleErrorBasedOnType(error);
    }

    private void handleErrorBasedOnType(VolleyError error) {

        String errorTitle = mContext.getResources().getString(R.string.generic_error_title);
        String errorMessage = mContext.getResources().getString(R.string.generic_error_message);

        if (error instanceof NoConnectionError) {

            mErrorType = WebRequestErrorType.NO_INTERNET_ERROR;
            SnackbarClient.showSnackBar(((Activity) mContext).
                            findViewById(android.R.id.content),
                    mContext.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG);
            if (null != mErrorListener) {
                mErrorListener.onErrorResponseReceived(mRequestType, mErrorType, error);
            }
        } else if (error instanceof TimeoutError) {
            mErrorType = WebRequestErrorType.TIMEOUT_ERROR;
            errorTitle = mContext.getResources().getString(R.string.timeout_title);
            errorMessage = mContext.getResources().getString(R.string.timeout_message);
            showGenericDialog(errorTitle, errorMessage);
        } else if (isServerProblem(error)) {
            errorTitle = mContext.getResources().getString(R.string.connection_failed);
            mErrorType = WebRequestErrorType.SERVER_ERROR;
            errorMessage = mContext.getResources().getString(R.string.connection_failed);
            showGenericDialog(errorTitle, errorMessage);
        } else if (error instanceof NetworkError) {
            mErrorType = WebRequestErrorType.NETWORK_ERROR;
            errorTitle = mContext.getResources().getString(R.string.connection_failed);
            errorMessage = mContext.getResources().getString(R.string.connection_failed);
            showGenericDialog(errorTitle, errorMessage);
        } else {
            mErrorType = WebRequestErrorType.GENERIC_ERROR;
            errorTitle = mContext.getResources().getString(R.string.generic_error_title);
            errorMessage = mContext.getResources().getString(R.string.generic_error_message);
            showGenericDialog(errorTitle, errorMessage);
        }

        Log.i(TAG, "Error Type in Error Helper:" + mErrorType);
    }

    /**
     * Display configurable generic dialog.
     *
     * @param title   - Title text
     * @param message - message text
     */
    private void showGenericDialog(String title, String message) {

        mGenericDialog = new GenericDialog(mContext);
        mGenericDialog.setOnButtonClickListener(mFailureDlgListener);
        mGenericDialog.setCancelable(false);
        mGenericDialog.setDialogTitle(title);
        mGenericDialog.setDialogMessage(message);
        mGenericDialog.setNegativeBtnVisibility(View.GONE);
        mGenericDialog.show();
    }
}