package com.ritesh.volleyapp.volleycode;

import com.ritesh.volleyapp.sample.apprequestconfig.GetPgRequestConfig;
import com.ritesh.volleyapp.sample.apprequestconfig.MetroCardValidateRequestConfig;
import com.ritesh.volleyapp.sample.apprequestconfig.PgAuthMethodRequestConfig;
import com.ritesh.volleyapp.sample.apprequestconfig.PgAuthRequestConfig;
import com.ritesh.volleyapp.sample.apprequestconfig.PgGetBalanceRequestConfig;
import com.ritesh.volleyapp.sample.apprequestconfig.TransactionHistoryRequestConfig;
import com.ritesh.volleyapp.volleycode.requestconfigs.IWebRequestConfig;

/**
 * Request name/identifiers.
 */
public enum WebRequestType {

    METRO_CARD_VALIDATE_REQ(new MetroCardValidateRequestConfig()),
    TRANSACTION_HISTORY_REQUEST(new TransactionHistoryRequestConfig()),
    PG_AUTH_METHODS_REQ(new PgAuthMethodRequestConfig()),
    PG_DO_AUTH_REQ(new PgAuthRequestConfig()),
    PG_GET_BALANCE_REQ(new PgGetBalanceRequestConfig()),
    GET_PAY_GATEWAYS_REQ(new GetPgRequestConfig());

    private IWebRequestConfig requestConfig;

    WebRequestType(IWebRequestConfig requestObject) {
        this.requestConfig = requestObject;
    }

    public IWebRequestConfig getRequestConfig() {
        return requestConfig;
    }
}
