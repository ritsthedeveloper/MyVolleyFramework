package com.ritesh.volleyapp.volleycode;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;

/**
 * Custom implementation of Volley Request Queue.
 * It is initialized with the application context, so that this queue last till the app lasts.
 *
 * @author Ritesh Gune.
 */
public class VolleyRequestQueue {

    /**
     * Log or request TAG
     */
    private static final String TAG = "VolleyRequestQueue";
    // Configurable number of concurrent requests
    private static final int NETWORK_THREAD_POOL_SIZE = 6;

    // volatile modifier prevents another thread in Java from
    // seeing half initialized state of instance.
    private volatile static VolleyRequestQueue mInstance;
    private Context mContext;
    // Queue for request.
    private RequestQueue mRequestQueue;

    private VolleyRequestQueue(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    /**
     * Double checked locking pattern is used to reduce the cost of using
     * synchronized. Synchronized is used to make it threadsafe.
     *
     * @param context - Application context.
     * @return
     */
    public static VolleyRequestQueue getInstance(Context context) {
        if (mInstance == null) {
            synchronized (VolleyRequestQueue.class) {
                if (mInstance == null) {
                    mInstance = new VolleyRequestQueue(context);
                }
            }
        }

        return mInstance;
    }

    /**
     * Get the request queue instance after initialising and starting it.
     * @return
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {

            /** Requestqueue with increased Cache size. - Custom implementation */
            Cache cache = new DiskBasedCache(mContext.getCacheDir(), 10 * 1024 * 1024); // 10 MB
            Network network = new BasicNetwork(new HurlStack());
            //Volley uses a PriorityBlockingQueue with a default capacity of 11 - waiting capacity 11.
            //max parallel network requests default 4.
            mRequestQueue = new RequestQueue(cache, network, NETWORK_THREAD_POOL_SIZE);
            /** Requestqueue with default implementation
             * Default cache is of 5 MB */
//            mRequestQueue = Volley.newRequestQueue(mCtx);
            // Don't forget to start the volley request queue
            mRequestQueue.start();
        }
        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        Log.i(TAG,"Add Request to Queue ----- : "+ req.getUrl());
        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        addToRequestQueue(req,TAG);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null && null != tag) {
            Log.i(TAG,"Cancel Request----- : "+ tag);
            mRequestQueue.cancelAll(tag);
        }
    }
}