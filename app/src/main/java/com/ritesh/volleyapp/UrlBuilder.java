package com.ritesh.volleyapp;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Ritesh <riteshg@birdseyetech.com>.
 */
public class UrlBuilder {

    private static final String TAG = "UrlBuilder";
    private String mBaseUrl;
    private Context mContext;

    public UrlBuilder(Context context) {
        mContext = context;
    }

    private Uri getBaseUri(Context context) {
        Uri baseUri = Uri.parse(mBaseUrl);
        return baseUri;
    }

    public String buildUrl(Context context, String baseUrl, List<String> pathParams, Map<String, String> queryParams) {

        if (null != baseUrl) {
            mBaseUrl = baseUrl;
        } else {
            mBaseUrl = context.getString(R.string.API_URL);
        }

        return buildUrl(context, pathParams, queryParams);
    }

    public String buildUrl(Context context, List<String> pathParams, Map<String, String> queryParams) {

        // Get builder with Base Url.
        Uri.Builder uriBuilder = getBaseUri(context).buildUpon();

        if (null != pathParams && 0 != pathParams.size()) {
            appendPathParams(uriBuilder, pathParams);
        }

        if (null != queryParams && 0 != queryParams.size()) {
            appendParams(uriBuilder, queryParams);
        }

        Uri builtUri = uriBuilder.build();
        String urlString = null;

        try {
            URL url = new URL(builtUri.toString());
            Log.i(TAG, "Built URL " + url.toString());
            return url.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return urlString;
    }

    private void appendPathParams(Uri.Builder uriBuilder, List<String> pathParams) {

        for (String singleParam : pathParams) {
            Log.d(TAG, "PathParam - Value :" + singleParam);
            uriBuilder.appendPath(singleParam);
        }
    }

    private void appendParams(Uri.Builder uriBuilder, Map<String, String> queryParams) {
        // If method has addition queryparams in map then add them to baseurl.
        Iterator it = queryParams.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry mapItem = (Map.Entry) it.next();
            Log.d(TAG, "QueryParam - Key :" + mapItem.getKey() + " :::: Value : " + mapItem.getValue());
            uriBuilder.appendQueryParameter(String.valueOf(mapItem.getKey()), String.valueOf(mapItem.getValue()));
        }
    }
}