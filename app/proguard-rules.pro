# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Ritesh\adt-bundle-windows-x86_64-20140702\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

## Preverification is irrelevant for the dex compiler and the Dalvik VM,
## so we can switch it off with the -dontpreverify option.
-dontpreverify

## When not preverifing in a case-insensitive filing system, such as Windows.
## Because this tool unpacks your processed jars, you should then use:
-dontusemixedcaseclassnames

# Specifies to write out some more information during processing.
# If the program terminates with an exception, this option will print out the entire stack trace,
# instead of just the exception message.
-verbose

## We're keeping annotations, since they might be used by custom RemoteViews.
-keepattributes *Annotation*

## This excludes shrinking and obfuscating parts of classes that extend select classes from the Android API.
## From the collective wisdom of Stack Overflow.
## Keep classes that are referenced on the AndroidManifest
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.preference.Preference
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.app.Fragment
-keep public class com.android.vending.billing.IInAppBillingService

#To maintain custom components names that are used on layouts XML:
-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.content.Context {
    public void *(android.view.View);
    public void *(android.view.MenuItem);
}

#Keep the R
-keepclassmembers class **.R$* {
    public static <fields>;
}

## If the library is open source, then there is no point to obfuscating
## its code from a security standpoint anyway.
## The official support library.

-dontwarn android.support.**
-keep class android.support.** { *; }
-keep interface android.support.** { *; }

#Maintain enums
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

#To keep parcelable classes (to serialize - deserialize objects to sent through Intents)
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

# contains classes and members excluded from obfuscation.
-printseeds seeds.txt

# using the printusage flag in Proguard.cfg, your configuration file,
# ProGuard will list the unused code to allow for proper code maintenance and cleanup.
-printusage unused.txt

# maps between the original and obfuscated class, method, and field names.
-printmapping mapping.txt
-keepattributes SourceFile,LineNumberTable

## To remove selective logging use this
-assumenosideeffects class android.util.Log {
    public static *** e(...);
    public static *** w(...);
    public static *** wtf(...);
    public static *** d(...);
    public static *** v(...);
}

## To remove all log calls, simply use this
# -assumenosideeffects class android.util.Log { *; }
